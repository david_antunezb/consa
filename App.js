import React, { useState, useEffect, useMemo } from 'react';
import { SafeAreaView, StyleSheet, StatusBar, LogBox } from 'react-native';
import { Provider as PaperProvider, DarkTheme as DarkThemePaper, DefaultTheme as DefaultThemePaper } from 'react-native-paper';

/**IMPORTACIONES DE COMPONENTES */
import Auth from './src/components/Auth';

/**IMPORTACIONES DE NAVIGATION */
import { NavigationContainer, DarkTheme as DarkThemeNavigation, DefaultTheme as DefaultThemeNavigation } from '@react-navigation/native'
import NavigationAdmin from './src/navigation/NavigationAdmin'
import NavigationEmpleado from './src/navigation/NavigationEmpleado'

/** EVITANDO AVISOS, ADVERTENCIAS (YellowBox) */
LogBox.ignoreLogs(["Picker has been extracted"]);
LogBox.ignoreLogs(['Setting a timer']);
LogBox.ignoreLogs(['Non-serializable values were found']);
LogBox.ignoreLogs(['Warning: Failed prop type:']);

/**IMPORTACIONES DE FIREBASE */
import firebase from './src/utils/firebase';
import 'firebase/firestore'
import { decode, encode } from 'base-64'
import 'firebase/auth';
//Acciones para Reconocer Firebase en Android
if (!global.btoa) global.btoa = encode;  //Para la base firebase
if (!global.atob) global.atob = decode;

firebase.firestore().settings({ experimentalForceLongPolling: true })
const db = firebase.firestore(firebase);

/**IMPORTANDO CONTEXT */
import PreferencesContext from './src/context/PreferencesContext';

export default function App() {
  /**HOOK DE ESTADOS */
  const [user, setUser] = useState(undefined)             //CONTENDRÁ LOS DATOS DEL USUARIO LOGUEADO
  const [tipoUsuario, setTipoUsuario] = useState('')
  const [theme, setTheme] = useState('dark')             //Implemente Modo Oscuro



  /**CONFIGURANDO COLORES DE LOS TEMAS */
  DefaultThemePaper.colors.primary = '#000';
  DarkThemePaper.colors.text = '#fff';       //Color de las Letras en menú y de los iconos y cuando se pincha
  DarkThemePaper.colors.primary = '#fff';    //Color de las letras cuando está seleecionado
  DarkThemePaper.colors.accent = '#fff';

  DarkThemeNavigation.colors.background = '#262626';
  DarkThemeNavigation.colors.card = '#3b5998';   //Facebook 3b5998     2f55a4    4e71ba

  const toggleTheme = () => {
    setTheme(theme === 'dark' ? 'light' : 'dark')
  }

  const preference = useMemo(  //Dar valores y que nuestra app modifique un componente, no se vuelva a su estado original
    () => ({
      toggleTheme,
      theme
    }),
    [theme],
  )

  /**COMPROBANDO SI EL USUARIO ESTÁ LOGEADO
   * Detecta cuando el Estado de usuario Cambia
  */
  useEffect(() => {
    firebase.auth().onAuthStateChanged((response) => {
      setUser(response);

      if (response != null) {
        var id = response.uid;
        db.collection('Usuarios').where("idUsuario", "==", id)
          .get()
          .then((mensaje) => {
            //console.log(mensaje);
            mensaje.forEach((doc) => {
              setTipoUsuario(doc.data().tipoUsuario)
              console.log(doc.data());
            });
          });
      }

    })
  }, []);

  if (user === undefined) return null;                   //NO CARGA NADA HASTA QUE NO SE TERMINE DE COMPROBAR SI ESTÁ LOGUEADO






  return (
    <>
      <PreferencesContext.Provider value={preference} >
        <PaperProvider theme={theme === 'dark' ? DarkThemePaper : DefaultThemePaper} >
          <StatusBar barStyle='light-content' />
          <NavigationContainer theme={theme === 'dark' ? DarkThemeNavigation : DefaultThemeNavigation}>
            <SafeAreaView style={styles.background}>
              {/** Si el usuario está logueado muestra la pantalla principal SINO manda a Loguearse o Registrarse */}
              {user ? (tipoUsuario == 'EMPLEADO' ? <NavigationEmpleado user={user} /> : <NavigationAdmin user={user} />) : <Auth />}

            </SafeAreaView>
          </NavigationContainer>

        </PaperProvider>

      </PreferencesContext.Provider>

    </>
  )
}

/** ESTILOS */
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',  //121111'
    height: '100%'
  },
});
