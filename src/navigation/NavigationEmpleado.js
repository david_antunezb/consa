import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

/**IMPORTACIONES PARA NAVEGACIÓN */
import { createDrawerNavigator } from '@react-navigation/drawer';
import StackNavigationEmpleado from './StackNavigationEmpleado'
const Drawer = createDrawerNavigator();

import DrawerEmpleado from './DrawerEmpleado'

export default function NavigationEmpleado(props) {
  const { user } = props;
  //console.log("Usuario recibido: ", user.uid);
  return (
    <Drawer.Navigator initialRouteName='homeEmpleado' drawerContent={(props) => <DrawerEmpleado {...props} />} >
      <Drawer.Screen name="Inicio" component={StackNavigationEmpleado} />
    </Drawer.Navigator>
  );

}
