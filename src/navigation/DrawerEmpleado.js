import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'
import { DrawerContentScrollView } from '@react-navigation/drawer'
import { Drawer, Switch, TouchableRipple, Text } from 'react-native-paper'
import usePreference from '../hooks/usePreferences'

import firebase from '../utils/firebase'

export default function DrawerEmpleado(props) {
  const { navigation } = props;
  const { theme, toggleTheme } = usePreference();
  /**HOOK DE ESTADOS */
  const [vistaActiva, setVistaActiva] = useState("home")

  const onChangeVista = (vista) => {//Se Encargará de renderizar la vista del menú que se seleccione
    setVistaActiva(vista);
    navigation.navigate(vista);
  }

  return (
    <DrawerContentScrollView>
      <Drawer.Section title='Menú'>
        <TouchableRipple>
          <Drawer.Item
            label="Inicio"
            active={vistaActiva == 'home'}  //Mostramos/Marcamos la vista que está activa
            icon='home'
            onPress={() => onChangeVista("home")}
          />
        </TouchableRipple>

        <Drawer.Item
          label="Eventos Convocados"
          active={vistaActiva == 'eventosConvocados'}
          icon='bell'
          onPress={() => onChangeVista("eventosConvocados")}
        />

        <Drawer.Item
          label="Mis Eventos"
          active={vistaActiva == 'misEventos'}
          icon='calendar'
          onPress={() => onChangeVista("misEventos")}
        />


        <Drawer.Item
          label="Mi Perfil"
          active={vistaActiva == 'miPerfil'}
          icon='account-group'
          onPress={() => onChangeVista("miPerfil")}
        />
      </Drawer.Section>

      <Drawer.Section title='Opciones'>
        <TouchableRipple>
          <View style={styles.preferences} >
            <Text>Modo Oscuro</Text>
            <Switch value={theme === 'dark'} onValueChange={toggleTheme} />
          </View>
        </TouchableRipple>

      </Drawer.Section>

      <Drawer.Section>
        <Drawer.Item
          label="Cerrar Sesión"
          icon='logout'
          onPress={() => firebase.auth().signOut()}
        />
      </Drawer.Section>

    </DrawerContentScrollView>
  )
}

const styles = StyleSheet.create({
  preferences: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 12,
    paddingHorizontal: 16
  }
})
