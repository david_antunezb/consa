import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import { createStackNavigator } from '@react-navigation/stack'


import HomeAdministrador from '../components/Administrador/HomeAdministrador'
import Eventos from '../components/Administrador/Eventos'
import SolicitudesPendientes from '../components/Administrador/SolicitudesPendientes'
import ConvocarElementos from '../components/Administrador/ConvocarElementos'
import LecturaQR from '../components/Administrador/LecturaQr'
import RegistrarPorQR from '../components/Administrador/RegistrarPorQR'

import { IconButton } from 'react-native-paper'

const Stack = createStackNavigator();



export default function StackNavigation(props) {
  const { navigation } = props;

  const botonIzquierdo = () => {
    return (
      <IconButton
        icon='menu'
        onPress={() => navigation.openDrawer()}
      />
    )
  }

  return (
    <Stack.Navigator>
      <Stack.Screen
        name='home'
        component={HomeAdministrador}
        options={{ title: 'CONSA App', headerLeft: () => botonIzquierdo() }}
      />

      <Stack.Screen
        name='eventos'
        component={Eventos}
        options={{ title: 'Eventos', headerLeft: () => botonIzquierdo() }}
      />

      <Stack.Screen
        name='solicitudesPendientes'
        component={SolicitudesPendientes}
        options={{ title: 'Solicitudes Pendientes', headerLeft: () => botonIzquierdo() }}
      />


      <Stack.Screen
        name='lecturaQR'
        component={LecturaQR}
        options={{ title: 'Registro de asistencia', headerLeft: () => botonIzquierdo() }}
      />


      <Stack.Screen
        component={ConvocarElementos}
        name='convocarElementos'
        options={{ title: 'Convocar Elementos', headerLeft: () => botonIzquierdo() }}
      />


      <Stack.Screen
        component={RegistrarPorQR}
        name='registroPorQR'
        options={{ title: 'Registro de Asistencia', headerLeft: () => botonIzquierdo() }}
      />

    </Stack.Navigator>
  )
}

const styles = StyleSheet.create({})
