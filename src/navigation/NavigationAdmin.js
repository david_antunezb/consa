import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

/**IMPORTACIONES PARA NAVEGACIÓN */
import { createDrawerNavigator } from '@react-navigation/drawer';
import StackNavigation from './StackNavigation'
const Drawer = createDrawerNavigator();

import DrawerAdmin from './DrawerAdmin';

export default function NavigationAdmin(props) {
  const { user } = props;

  return (
    <Drawer.Navigator initialRouteName='homeAdministrador' drawerContent={(props) => <DrawerAdmin {...props} />} >
      <Drawer.Screen name="Inicio" component={StackNavigation} />
    </Drawer.Navigator>
  );

}