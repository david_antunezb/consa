import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import { createStackNavigator } from '@react-navigation/stack'

import HomeEmpleado from '../components/Empleado/HomeEmpleado'
import EventosConvocados from '../components/Empleado/EventosConvocados'
import MisEventos from '../components/Empleado/MisEventos'
import MiPerfil from '../components/Empleado/MiPerfil'

import { IconButton } from 'react-native-paper'
const Stack = createStackNavigator();

export default function StackNavigationEmpleado(props) {

  const { navigation } = props;

  const botonIzquierdo = () => {
    return (
      <IconButton
        icon='menu'
        onPress={() => navigation.openDrawer()}
      />
    )
  }

  return (
    <Stack.Navigator>
      <Stack.Screen
        name='home'
        component={HomeEmpleado}
        options={{ title: 'CONSA App', headerLeft: () => botonIzquierdo() }}
      />

      <Stack.Screen
        name='eventosConvocados'
        component={EventosConvocados}
        options={{ title: 'Eventos Convocados', headerLeft: () => botonIzquierdo() }}
      />

      <Stack.Screen
        name='misEventos'
        component={MisEventos}
        options={{ title: 'Mis Eventos', headerLeft: () => botonIzquierdo() }}
      />

      <Stack.Screen
        name='miPerfil'
        component={MiPerfil}
        options={{ title: 'Mi Perfil', headerLeft: () => botonIzquierdo() }}
      />

    </Stack.Navigator>
  )
}

const styles = StyleSheet.create({})
