import firebase from 'firebase/app';

/** Conexión de la Aplicación CONSA con FIREBASE
 * Constante de Configuración de Firebase
 * Cuenta: Antunez
 */
const firebaseConfig = {
  apiKey: "AIzaSyAp8Jyo15hJLej7tU8_tRPpsYwipwfhpdg",
  authDomain: "consa-4b07b.firebaseapp.com",
  databaseURL: "https://consa-4b07b.firebaseio.com",
  projectId: "consa-4b07b",
  storageBucket: "consa-4b07b.appspot.com",
  messagingSenderId: "103548124917",
  appId: "1:103548124917:web:27222ae86f0725b6220fbc"
};

/** Se inicializa Firebase */
export default firebase.initializeApp(firebaseConfig);
