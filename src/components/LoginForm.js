import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native'

/**IMPORTACIONES DE FIREBASE */
import firebase from '../utils/firebase'

export default function LoginForm(props) {
  /**HACIENDO DESTRUCTURING */
  const { changeForm } = props;

  /**HOOK DE ESTADOS */
  const [datosFormulario, setDatosFormulario] = useState(valoresDefault());
  const [erroresFormulario, setErroresFormulario] = useState({});           //Contiene banderas para cada campo, True: Error, False: Correcto
  const [mensajeError, setMensajeError] = useState('')

  const onChange = (e, type) => { /** Función que guarda los datos del formulario en el Estado */
    setDatosFormulario({ ...datosFormulario, [type]: e.nativeEvent.text })

  };

  const loguearUsuario = () => {
    let errores = {};
    setErroresFormulario(errores);

    if (!datosFormulario.email || !datosFormulario.password) {
      if (!datosFormulario.email) errores.email = true;
      if (!datosFormulario.password) errores.password = true;

    }

    if (errores.email == true) {
      setMensajeError("Ingresa tu email");
    } else if (errores.password == true) {
      setMensajeError("Ingresa tu contraseña");
    } else { //SI TODO va bien se loguea al Usuario
      setMensajeError("");
      console.log("Iniciando Sesión.....");
      firebase.auth()
        .signInWithEmailAndPassword(datosFormulario.email.trim(), datosFormulario.password)
        .then(() => {
          console.log("Ha iniciado sessión");
        })
        .catch(() => {
          setMensajeError('Credenciales Incorrectas!');
          setErroresFormulario({
            email: true,
            password: true
          })
        })
    }

    setErroresFormulario(errores);

  }

  return (
    <>

      <Text style={styles.mensajeError} > {mensajeError} </Text>

      {/** Campo para Correo Electrónico del Empleado */}
      <TextInput
        style={[styles.input, erroresFormulario.email && styles.errorInput]} //Si erroresFormulario es True &&(Entonces) aplica el estilo
        placeholder='Correo Electrónico'
        placeholderTextColor='#807e7e'
        onChange={(e) => onChange(e, 'email')}
      />

      {/** Campo para Contraseña del Empleado */}
      <TextInput
        style={[styles.input, erroresFormulario.password && styles.errorInput]}
        placeholder='Contraseña'
        placeholderTextColor='#807e7e'
        secureTextEntry={true}
        onChange={(e) => onChange(e, 'password')}
      />

      {/** Botón para Logear al usuario */}
      <TouchableOpacity onPress={loguearUsuario} >
        <Text style={styles.btnLogin}>Iniciar Sesión</Text>
      </TouchableOpacity>


      {/** Botón para Redireccionar al Formulario de Registro*/}
      <View style={styles.btnTextRegister}>
        <TouchableOpacity onPress={changeForm}>
          <Text style={styles.btnText} >¿No tienes una cuenta? Registrate</Text>
        </TouchableOpacity>
      </View>
    </>
  )
}

function valoresDefault() { /** Inicializa los valores que se recibirán del Formulario al Hook de Estado */
  return {
    email: '',
    password: ''
  }
}

const styles = StyleSheet.create({
  btnTextRegister: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 50,
  },
  input: {
    height: 50,
    color: '#000',
    width: '80%',
    marginBottom: 20,
    backgroundColor: '#f2f5f7',
    paddingHorizontal: 20,
    borderRadius: 50,
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#000'
  },
  btnText: {
    color: '#3b5998',
    fontSize: 16,
    fontWeight: "bold",
  },
  btnLogin: {
    height: 50,
    paddingTop: 10,
    color: '#fff',
    paddingHorizontal: 20,
    fontSize: 18,
    backgroundColor: '#3b5998',  //0c527d   //Color Naranja: f28f24
    width: '100%',
    borderRadius: 50,
    alignContent: 'center',
    borderColor: '#000',
    fontWeight: "bold",
  },
  errorInput: {
    borderColor: '#ff000d',
    borderWidth: 2,
  },
  mensajeError: {
    textAlign: 'center',
    color: '#f00',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 5
  }
})
