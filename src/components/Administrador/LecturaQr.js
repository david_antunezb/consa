'use strict';
import React, { Component, useState, useEffect } from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
  ScrollView,
  ActivityIndicator
} from 'react-native';



import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';

import moment from 'moment';
import { Provider as PaperProvider, Title, List, Colors } from 'react-native-paper'
import { Card } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native'
import { IconButton } from 'react-native-paper'
import { Button, Icon } from 'native-base';

/**IMPORTACIÓN DE COMPONENTES */
import AddEvent from './AddEvent'
import Evento from './RegistroPorEvento'

/**IMPORTACIONES DE FIREBASE */
import firebase from '../../utils/firebase'
import 'firebase/firestore';

//Acciones para Reconocer Firebase en Android
firebase.firestore().settings({ experimentalForceLongPolling: true })


export default function LecturaQr({ navigation }) {
  const onSuccess = e => {
    console.log("MENSAJE LEIDO: " + e.data);
    /*Linking.openURL(e.data).catch(err =>
      console.error('An error occured', err)
    );*/
  };
  //Instanciamos la Base FIREBASE
  const db = firebase.firestore(firebase)
  /**HOOKS DE ESTADO */
  const [showEventos, setShowEventos] = useState(true) //Muestra la lista de Eventos
  const [eventos, setEventos] = useState([])  //Contiene todos los Eventos de la base
  const [eventosPasados, setEventosPasados] = useState([])
  const [reloadData, setReloadData] = useState(false);


  useEffect(() => {  //Obtiene TODOS los Eventos
    setEventos([]);
    setEventosPasados([]);
    db.collection('Eventos')
      .orderBy('fechaEvento', 'asc') //Primero las mas cercanas
      .get()
      .then((response) => {
        const itemsArray = []; //Se guardará la inforación obtenida
        response.forEach((doc) => {
          const data = doc.data();
          data.id = doc.id; //Obtenemos el id del Documento (Sirve por si queremos eliminar)

          itemsArray.push(data);
        });
        separarEventos(itemsArray);
      });
    setReloadData(false);
  }, [reloadData])

  const separarEventos = (items) => {
    const fechaActual = moment().set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0
    });

    const arrayTempEventos = [];
    const arrayTempEventosPasados = [];

    items.forEach((item) => {
      const fecha = new Date(item.fechaEvento.seconds * 1000);
      const fechaEvento = moment(fecha);
      const anioActual = moment().get('year');
      fechaEvento.set({ year: anioActual });


      const diferenciaFecha = fechaActual.diff(fechaEvento, 'days')
      const itemTemp = item;

      itemTemp.fechaEvento = fechaEvento;
      itemTemp.days = diferenciaFecha;

      if (diferenciaFecha <= 0) {  //Los 0 o menos a cero -10 Significa que son los que estan por venir o es hoy el cumpleaños
        arrayTempEventos.push(itemTemp);
      } else {
        arrayTempEventosPasados.push(itemTemp);
      }
    })

    setEventos(arrayTempEventos);
    setEventosPasados(arrayTempEventosPasados);
  }

  // debe recibir el evento desde el hijo
  const irAConvocatoria = (evento) => {
    navigation.navigate('registroPorQR', { evento });
  }




  return (
    <>
      <PaperProvider>
        {/** Botón para Crear un Evento */}
        <Title style={[styles.textThemeDark, styles.title]}>Eventos</Title>

        {/** RARA COMBINACIÓN DE native-base y paper */}
        {eventos.length > 0 || eventosPasados.length > 0 ?
          <>
            {showEventos ? (
              <ScrollView style={styles.scrollView}>
                {/**EVENTOS ACTUALES O POR VENIR */}
                { eventos.map((item, index) => (
                  <Evento key={index} evento={item} irAConvocatoria={irAConvocatoria} setReloadData={setReloadData} />
                ))}
              </ScrollView>
            ) : (
                <AddEvent setShowEventos={setShowEventos} setReloadData={setReloadData} />
              )
            }
          </>
          :
          <>
            {showEventos ? (
              <View style={styles.viewContainer} >
                <ActivityIndicator size="large" color="#fff" />
                {/** <Text style={styles.TitleNotFound} > ¡Lo sentimos!</Text>
              <Text style={styles.NotFoundEventos} > No Hay Eventos A Mostrar</Text> */}
              </View>
            ) :
              (
                <AddEvent setShowEventos={setShowEventos} setReloadData={setReloadData} />
              )
            }
          </>
        }

      </PaperProvider>
      <Text></Text>



    </>


  )
}

const styles = StyleSheet.create({
  textThemeDark: {
    color: '#fff',
  },
  title: {
    width: '90%',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 20,
    marginLeft: 20,
    borderBottomColor: '#fff',
    borderBottomWidth: 1,

  },
  container: {
    height: '100%',
    //alignItems: 'center',
  },
  textThemeDark: {
    color: '#fff',
  },
  viewAdd: {
    backgroundColor: '#3b5998',
    borderRadius: 50,
    paddingVertical: 10,
    paddingHorizontal: 30
  },
  title: {
    width: '90%',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 20,
    marginLeft: 20,
    borderBottomColor: '#fff',
    borderBottomWidth: 1,

  },
  scrollView: {
    width: '100%',
    marginTop: 30,
    marginBottom: 50,
    alignContent: 'center'
  },
  row: {
    flexDirection: 'row',
    width: '60%',
  },
  botonAdd: {
    fontSize: 16,
    color: '#fff',
    backgroundColor: '#3b5998',   //Naranja: f28f24   Azul 0c527d
    width: '60%',
    height: 50,
    paddingTop: 10,
    paddingHorizontal: 20,
    borderRadius: 50,
    alignContent: 'center',
    borderColor: '#fff',
  },
  boton: {
    position: 'absolute',
    top: 70,
    right: 20,
    width: '45%',
    paddingRight: 20,
    backgroundColor: '#3b5998',
    borderRadius: 50,
  },
  NotFoundEventos: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  },
  TitleNotFound: {
    color: '#fff',
    fontSize: 35,
    fontWeight: 'bold',
  },
  viewContainer: {
    marginTop: 220,
    alignItems: 'center',
  },
  load: {
    color: '#000'
  }

})
