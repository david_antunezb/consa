import React, { useEffect, useState } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {
  Provider as PaperProvider,
  List,
  Badge,
  Title,
  Card
} from 'react-native-paper';

import firebase from '../../utils/firebase';
import ModalSolicitud from './ModalSolicitud';

export default function SolicitudesPendientes() {
  const db = firebase.firestore(firebase);
  const [usuarios, setUsuarios] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [usuarioSeleccionado, setUsuarioSeleccionado] = useState(null);
  const [cambioPeticiones, setCambioPeticiones] = useState(false);
  const [cargando, setCargando] = useState(true);

  // recuperando las solicitudes pendientes
  useEffect(() => {
    const arregloUsuarios = [];

    db.collection('Usuarios')
      .where('estadoSolicitud', '==', 'PENDIENTE')
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          const usr = doc.data();
          usr.id = doc.id;
          arregloUsuarios.push(usr);
        });
        setUsuarios(arregloUsuarios);
        setCargando(false);
      })
      .catch(function (error) {
        console.log('Error al obtener usuarios');
      });
  }, [cambioPeticiones]);

  const renderizarLista = () => {
    if (usuarios.length > 0) {
      const fields = [];
      for (let i = 0; i < usuarios.length; i++) {
        fields.push(
          <List.Item
            onPress={() => {
              mostrarModal(usuarios[i]);
            }}
            key={'list_' + i}
            style={styles.listItem}
            title={
              usuarios[i].nombre +
              ' ' +
              usuarios[i].aPaterno +
              ' ' +
              usuarios[i].aMaterno
            }
            right={(props) => <List.Icon {...props} icon="eye" />}
          />,
        );
      }
      return fields;
    } else {
      return (
        <List.Item
          style={styles.itemEmpty}
          title={'No hay solicitudes por atender'}
          right={(props) => <List.Icon {...props} icon="check" />}
        />
      )
    }
  };

  const mostrarModal = (usuario) => {
    setUsuarioSeleccionado(usuario);
    setShowModal(true);
  };

  return (
    <PaperProvider>
      {cargando ? (
        <View style={styles.viewActivity}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      ) : usuarios && !cargando ? (
        <>
          <ModalSolicitud
            cambioPeticiones={cambioPeticiones}
            setCambioPeticiones={setCambioPeticiones}
            showModal={showModal}
            setShowModal={setShowModal}
            usuarioSeleccionado={usuarioSeleccionado}
          />

          <View style={styles.contenedorTitulo}>
            <Title style={styles.title}>Solicitudes Pendientes:</Title>
            <Badge size={30}>{usuarios.length}</Badge>
          </View>

          <ScrollView>
            <View style={styles.viewContainer}>{renderizarLista()}</View>
          </ScrollView>
        </>
      ) : (
            <></>
          )}
    </PaperProvider>
  );
}

const styles = StyleSheet.create({
  viewClose: {
    backgroundColor: '#B20000',
    borderRadius: 50,
    paddingVertical: 10,
    paddingHorizontal: 30,
  },
  contenedorTitulo: {
    width: '90%',
    flexDirection: 'row',
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 10,
    borderBottomColor: '#fff',
    borderBottomWidth: 1,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#fff',
    marginRight: 10,
  },
  viewContainer: {
    padding: 20,
  },
  viewActivity: {
    marginTop: '50%',
    alignItems: 'center',
  },
  listItem: {
    backgroundColor: '#f2f5f7',
    borderRadius: 20,
    marginBottom: 10,
  },
  itemEmpty: {
    backgroundColor: '#97d1a2',
    borderRadius: 5,
    marginBottom: 10,
  }
});
