import React, {useEffect, useState} from 'react';
import {StyleSheet, ActivityIndicator, View, PermissionsAndroid, Platform} from 'react-native';
import {
  Provider as PaperProvider,
  Title,
  List,
  Button,
  Snackbar,
} from 'react-native-paper';
import {CheckBox} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import RNHTMLtoPDF from 'react-native-html-to-pdf';

import firebase from '../../utils/firebase';
import Evento from './Evento';
import { logotipoBase64 } from '../../utils/logo-base64';

export default function ConvocarElementos(props) {
  const {evento} = props.route.params;
  const {navigation} = props;
  const db = firebase.firestore(firebase);

  const [usuarios, setUsuarios] = useState(null);
  const [total, setTotal] = useState(0);
  const [usuariosEvento, setUsuariosEvento] = useState(null);
  const [cargando, setCargando] = useState(true);

  const [showSnack, setShowSnack] = useState(false);
  const onToggleSnackBar = () => setShowSnack(!showSnack);
  const onDismissSnackBar = () => setShowSnack(false);

  useEffect(() => {
    // obtener el registro de la coleccion UsuariosEventos cuyo id coincida con el del evento actual
    db.collection('UsuariosEventos')
      .where('idEvento', '==', evento.id)
      .get()
      .then((querySnapshot) => {
        if (querySnapshot.size > 0) {
          const convocatoria = querySnapshot.docs[0].data();
          setUsuariosEvento(convocatoria);
          obtenerUsuariosConvocados(convocatoria);
        } else {
          obtenerUsuarios();
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }, [evento]);

  const obtenerUsuarios = () => {
    setTotal(0);
    const arregloUsuarios = [];

    db.collection('Usuarios')
      .where('estadoSolicitud', '==', 'CONFIRMADA')
      .get()
      .then((querySnapshot) => {
        let contador = 0;

        querySnapshot.forEach(function (doc) {
          const usr = doc.data();
          usr.id = doc.id;
          usr.seleccionado = usr.puesto == 'Encargado' ? true : false;
          arregloUsuarios.push(usr);

          if (usr.seleccionado) {
            contador++;
          }
        });
        setTotal(contador);
        setUsuarios(arregloUsuarios);
        setCargando(false);
      })
      .catch((error) => {
        console.log(error, 'Error al obtener usuarios');
      });
  };

  const obtenerUsuariosConvocados = (convocatoria) => {
    setTotal(convocatoria.usuariosEvento.length);
    const arregloUsuarios = [];

    for (let i=0; i<convocatoria.usuariosEvento.length; i++) {
      db.collection('Usuarios')
        .doc(convocatoria.usuariosEvento[i].idUsuario)
        .get()
        .then((querySnapshot) => {
          const usuario = querySnapshot.data();
          arregloUsuarios.push(usuario);
          setUsuarios(arregloUsuarios);
        }).catch((e) => {
          console.log(e);
        });
      setTimeout(() => {
        setCargando(false);
      }, 500);
    }
  };

  const onChangeCheck = (u) => {
    if (u.seleccionado) {
      setTotal(total - 1);
    } else {
      setTotal(total + 1);
    }

    u.seleccionado = !u.seleccionado;
    const nuevosUsuarios = [];
    for (let i = 0; i < usuarios.length; i++) {
      nuevosUsuarios.push(usuarios[i]);
    }
    setUsuarios(nuevosUsuarios);
  };

  const guardarConvocados = () => {
    const usuariosEvento = [];
    for (let i = 0; i < usuarios.length; i++) {
      if (usuarios[i].seleccionado) {
        const usuario = {
          idUsuario: usuarios[i].id,
          statusConvocatoria: 'PENDIENTE'
        };
        usuariosEvento.push(usuario);
      }
    }

    db.collection('UsuariosEventos')
      .add({
        usuariosEvento,
        idEvento: evento.id
      })
      .then(() => {
        onToggleSnackBar();
        setTimeout(() => {
          navigation.goBack();
        }, 1500);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const isPermitted = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs access to Storage data',
          },
        );
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        alert('Write permission err', err);
        console.log(err);
        return false;
      }
    } else {
      return true;
    }
  };

  const generarPdf = async () => {
    if (await isPermitted()) {
      let options = {
        html: html(),
        fileName: `convocatoria_evento_${evento.idEvento}`,
        directory: 'Documents'
      };

      RNHTMLtoPDF.convert(options).then(file => {
        alert('Archivo Creado En: ' + file.filePath);
      }).catch(err => {
        alert('Ha ocurrido un error!: ', err);
        console.log(err);
      });
    }
  };

  const html = () => {
    const fechaActual = new Date().toISOString().split('T')[0];
    const fechaEvento = new Date(evento.fechaEvento.toDate()).toISOString().split('T')[0];

    let htmlContent = `
      <style>
        table.tabla-elementos, table.tabla-elementos td, table.tabla-elementos th {
          border: 1px solid black;
        }
        table {
          border-collapse: collapse;
          width: 100%;
        }
        img {
          width: 75px;
        }
      </style>

      <table>
        <tbody>
          <tr>
            <td style="width: 15%;">
              <img src="data:image/png;base64,${logotipoBase64}" />
            </td>
            <td style="text-align: left; width: 15%;">CONSA APP</td>
            <td style="text-align: right;">${fechaActual}</td>
          </tr>
        </tbody>
      </table>

      <h3>Datos Del Evento</h3>
      <table class="tabla-elementos">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Lugar</th>
            <th>Fecha</th>
            <th>Total Elementos</th>
          </tr>
        </thead>
      
        <tbody>
          <tr>
            <td>${evento.nombreEvento}</td>
            <td>${evento.lugarEvento}</td>
            <td>${fechaEvento}</td>
            <td>${evento.numeroElementos}</td>
          </tr>
        </tbody>
      </table>

      <h3>Elementos Convocados</h3>
      <table class="tabla-elementos">
        <thead>
          <th>Nombre</th>
          <th>Puesto</th>
          <th>Zona Trabajo</th>
          <th>Email</th>
        </thead>
        
        <tbody>
    `;

    for (let i=0; i<usuarios.length; i++) {
      htmlContent += `
        <tr>
          <td>
            ${usuarios[i].nombre} ${usuarios[i].aPaterno} ${usuarios[i].aMaterno}
          </td>
          <td>
            ${usuarios[i].puesto}
          </td>
          <td>
            ${usuarios[i].zonaTrabajo}
          </td>
          <td>
            ${usuarios[i].email}
          </td>
        </tr>
      `;
    }

    htmlContent += `
          </tbody>
        </table>
      </html>
    `
    return htmlContent;
  }

  const renderizarBotonGuardar = () => {
    if (usuariosEvento) {
      return (
        <Button
          onPress={() => generarPdf()}
          style={styles.button}
          mode="contained"
          icon="file-pdf">
          Generar PDF
        </Button>
      );
    } else {
      return (
        <Button
          onPress={() => guardarConvocados()}
          style={styles.button}
          mode="contained"
          icon="send">
          Convocar
        </Button>
      );
    }
  }

  return (
    <PaperProvider>
      {/** Notificacion mostrada al atender una peticion */}
      <Snackbar
        style={styles.snack}
        visible={showSnack}
        onDismiss={onDismissSnackBar}
        action={{
          label: 'Aceptar',
          onPress: () => {},
        }}>
        Elementos Convocados Correctamente!
      </Snackbar>

      <Title style={styles.title}>Convocar Elementos</Title>

      {/* Datos generales del evento */}
      <Evento evento={evento}></Evento>

      {/* Contenedor para convocar a los empleados */}
      {
        cargando ? (
          <View style={styles.viewContainer}>
            <ActivityIndicator size="large" color="#fff" />
          </View>
        ) :
        (
          <>
            <Title style={styles.subtitle}>Total Elementos Convocados: {total}</Title>
            <ScrollView style={styles.viewEmpleados}>
              <List.Section>
                {/* Acordeon Cancha */}
                <List.Accordion
                  title="Cancha"
                  left={(props) => <List.Icon {...props} icon="soccer-field" />}>
                  {usuarios?.map((u, index) => {
                    if (u.zonaTrabajo == 'Cancha' && !usuariosEvento)
                      return (
                        <CheckBox
                          key={index}
                          title={`${u.nombre} ${u.aPaterno} ${u.aMaterno}`}
                          checked={u.seleccionado}
                          onPress={() => onChangeCheck(u)}
                        />
                      );
                    else if (u.zonaTrabajo == 'Cancha' && usuariosEvento) {
                      return (
                        <List.Item
                          key={index}
                          title={`${u.nombre} ${u.aPaterno} ${u.aMaterno}`}
                          left={(props) => <List.Icon {...props} icon="check" />}
                        />
                      );
                    }
                  })}
                </List.Accordion>

                {/* Acordeon General */}
                <List.Accordion
                  title="General"
                  left={(props) => <List.Icon {...props} icon="map" />}>
                  {usuarios?.map((u, index) => {
                    if (u.zonaTrabajo == 'General' && !usuariosEvento)
                      return (
                        <CheckBox
                          key={index}
                          title={`${u.nombre} ${u.aPaterno} ${u.aMaterno}`}
                          checked={u.seleccionado}
                          onPress={() => onChangeCheck(u)}
                        />
                      );
                    else if (u.zonaTrabajo == 'General' && usuariosEvento) {
                      return (
                        <List.Item
                          key={index}
                          title={`${u.nombre} ${u.aPaterno} ${u.aMaterno}`}
                          left={(props) => <List.Icon {...props} icon="check" />}
                        />
                      );
                    }
                  })}
                </List.Accordion>

                {/* Acordeon Palcos */}
                <List.Accordion
                  title="Palcos"
                  left={(props) => <List.Icon {...props} icon="chair-rolling" />}>
                  {usuarios?.map((u, index) => {
                    if (u.zonaTrabajo == 'Palcos' && !usuariosEvento)
                      return (
                        <CheckBox
                          key={index}
                          title={`${u.nombre} ${u.aPaterno} ${u.aMaterno}`}
                          checked={u.seleccionado}
                          onPress={() => onChangeCheck(u)}
                        />
                      );
                    else if (u.zonaTrabajo == 'Palcos' && usuariosEvento) {
                      return (
                        <List.Item
                          key={index}
                          title={`${u.nombre} ${u.aPaterno} ${u.aMaterno}`}
                          left={(props) => <List.Icon {...props} icon="check" />}
                        />
                      );
                    }
                  })}
                </List.Accordion>

                {/* Acordeon Preferente */}
                <List.Accordion
                  title="Preferente"
                  left={(props) => <List.Icon {...props} icon="star" />}>
                  {usuarios?.map((u, index) => {
                    if (u.zonaTrabajo == 'Preferente' && !usuariosEvento)
                      return (
                        <CheckBox
                          key={index}
                          title={`${u.nombre} ${u.aPaterno} ${u.aMaterno}`}
                          checked={u.seleccionado}
                          onPress={() => onChangeCheck(u)}
                        />
                      );
                    else if (u.zonaTrabajo == 'Preferente' && usuariosEvento) {
                      return (
                        <List.Item
                          key={index}
                          title={`${u.nombre} ${u.aPaterno} ${u.aMaterno}`}
                          left={(props) => <List.Icon {...props} icon="check" />}
                        />
                      );
                    }
                  })}
                </List.Accordion>

                {/* Acordeon Tribuna Diablos */}
                <List.Accordion
                  title="Tribuna Diablos"
                  left={(props) => <List.Icon {...props} icon="trumpet" />}>
                  {usuarios?.map((u, index) => {
                    if (u.zonaTrabajo == 'Tribuna Diablos' && !usuariosEvento)
                      return (
                        <CheckBox
                          key={index}
                          title={`${u.nombre} ${u.aPaterno} ${u.aMaterno}`}
                          checked={u.seleccionado}
                          onPress={() => onChangeCheck(u)}
                        />
                      );
                    else if (u.zonaTrabajo == 'Tribuna Diablos' && usuariosEvento) {
                      return (
                        <List.Item
                          key={index}
                          title={`${u.nombre} ${u.aPaterno} ${u.aMaterno}`}
                          left={(props) => <List.Icon {...props} icon="check" />}
                        />
                      );
                    }
                  })}
                </List.Accordion>
              </List.Section>
            </ScrollView>  
            {renderizarBotonGuardar()}
          </> 
        )
      }
    </PaperProvider>
  );
}

const styles = StyleSheet.create({
  title: {
    width: '90%',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 20,
    marginLeft: 20,
    borderBottomColor: '#fff',
    borderBottomWidth: 1,
    color: '#fff',
  },
  subtitle: {
    width: '90%',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 10,
    marginLeft: 20,
    borderBottomColor: '#fff',
    borderBottomWidth: 1,
    color: '#fff',
  },
  viewEmpleados: {
    backgroundColor: '#f4f5f8',
    borderRadius: 15,
    marginHorizontal: 10,
  },
  viewContainer: {
    marginTop: '50%',
    alignItems: 'center',
  },
  button: {
    marginVertical: 10,
    marginHorizontal: 10,
    backgroundColor: '#3b5998',
    borderRadius: 5,
  },
  viewSnack: {
    alignContent: 'center',
    justifyContent: 'center',
  },
});
