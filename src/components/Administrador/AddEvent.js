import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity } from 'react-native'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import RNPickerSelect from 'react-native-picker-select';
import moment from 'moment';


/**IMPORTACIONES DE FIREBASE */
import firebase from '../../utils/firebase'
import 'firebase/auth';
import 'firebase/firestore';

//Acciones para Reconocer Firebase en Android
firebase.firestore().settings({ experimentalForceLongPolling: true })

//Instanciamos la Base FIREBASE
const db = firebase.firestore(firebase)

export default function AddEvent(props) {
  const { setShowEventos, setReloadData } = props;

  /**HOOK DE ESTADOS */
  const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);    //Hace visible el Calendario
  const [datosFormulario, setDatosFormulario] = useState(valoresDefault())  //Contiene los datos del formulario
  const [erroresFormulario, setErroresFormulario] = useState({});           //Contiene banderas para cada campo, True: Error, False: Correcto
  const [mensajeError, setMensajeError] = useState('');

  /** * * * * *  DEFINICIÓN DE FUNCIONES * * * * * */
  const hideDatePicker = () => { /**Oculta el Datepicker */
    setIsDatePickerVisible(false);
  };

  const showDatePicker = () => { /** Muestra el Datepicker */
    setIsDatePickerVisible(true);
  };

  const handlerConfirm = (date) => {/** Selecciona la fecha y se oculta el calendario */
    const fecha = date;
    fecha.setHours(0);
    fecha.setMinutes(0);
    fecha.setSeconds(0);

    setDatosFormulario({ ...datosFormulario, fechaEvento: fecha });
    hideDatePicker();
  };

  const onChange = (e, type) => { /** Función que guarda los datos del formulario en el Estado */
    setDatosFormulario({ ...datosFormulario, [type]: e.nativeEvent.text })

  };

  const registrarEvento = () => {
    console.log(datosFormulario);
    let errores = {};
    setErroresFormulario(errores);

    if (!datosFormulario.nombreEvento || !datosFormulario.fechaEvento || !datosFormulario.numeroElementos) {  //Si un campo es Vacío Entra

      if (!datosFormulario.nombreEvento) errores.nombreEvento = true;     //Si el campo esta vacío asigno True: Hay Error
      if (!datosFormulario.fechaEvento) errores.fechaEvento = true;
      if (!datosFormulario.numeroElementos) errores.numeroElementos = true;

    }

    if (errores.nombreEvento == true) {
      setMensajeError("Ingresa nombre del evento");
    } else if (errores.fechaEvento == true) {
      setMensajeError("Ingresa fecha del evento");
    } else if (datosFormulario.lugarEvento == 'Nulo') {
      setMensajeError("Ingresa lugar del evento");
    } else if (errores.numeroElementos == true) {
      setMensajeError("Ingresa # Elementos de seguridad");
    } else if (datosFormulario.numeroElementos == 0 || datosFormulario.numeroElementos == '.') {
      setMensajeError("# de Elementos de seguridad inválido");
    }
    else {  //Lógica para registrar un evento
      setMensajeError("")
      console.log("Registrando.....");

      creandoID();

      agregarEvento(datosFormulario);

    }

    //Si hay errores se setea el Hook de Estado
    setErroresFormulario(errores);
  }

  const creandoID = () => {
    const data = datosFormulario;

    //GENERAMOS ID CON: nombreEvento, FechaEvento, Lugar
    var nombreE = data.nombreEvento.replace(/ /g, "");  //QUITAMOS ESPACIOS AL NOMBRE EVENTO
    var fechaE = (moment(data.fechaEvento).format('LL')).replace(/ /g, "");  //A LA FECHA
    var lugar = data.lugarEvento;  //OBTENEMOS LUGAR
    var id = nombreE + fechaE + lugar;

    data.idEvento = id;  //Asignamos al datosFormulario

    data.fechaEvento.setYear(0);   //Seteamos Año para lectura facil
    console.log("ID: " + datosFormulario.idEvento);
    console.log(data);
  }

  const agregarEvento = (datosFormulario) => {
    data = datosFormulario;
    db.collection('Eventos')   //Colección a la que voy a setear
      .add({
        idEvento: data.idEvento,
        nombreEvento: data.nombreEvento,
        fechaEvento: data.fechaEvento,
        lugarEvento: data.lugarEvento,
        numeroElementos: data.numeroElementos
      })
      .then(() => {
        console.log("Se Registró con Éxito a la Tabla Eventos");
        setReloadData(true); //Es para recargar cada vez que se agrega un nuevo evento
        setShowEventos(true);
      })
      .catch((mensaje) => {
        console.log("ERROR");
        console.log(mensaje);
      })
  }

  return (
    <>
      <ScrollView style={styles.scrollView}>
        <View style={styles.view} >
          <Image
            style={styles.logo}
            source={require('../../assets/img/LogoConsa.png')}
          />
        </View>

        <Text style={styles.mensajeError} > {mensajeError} </Text>

        {/** Campo para Nombre del Evento */}
        <TextInput
          style={[styles.input, erroresFormulario.nombreEvento && styles.errorInput]} //Si erroresFormulario es True &&(Entonces) aplica el estilo
          placeholder='Nombre del Evento'
          placeholderTextColor='#807e7e'
          onChange={(e) => onChange(e, 'nombreEvento')}
        />

        {/** Campo para Fecha del Evento*/}
        <View style={[styles.input, styles.datepicker, erroresFormulario.fechaEvento && styles.errorInput]}>
          <Text style={{
            color: datosFormulario.fechaEvento ? '#000' : '#969696',
            fontSize: 18
          }} onPress={showDatePicker}>
            {datosFormulario.fechaEvento
              ? moment(datosFormulario.fechaEvento).format('LL')
              : 'Fecha del Evento'
            }
          </Text>
        </View>

        {/** Campo para Seleccionar zona de trabajo del Empleado */}
        <RNPickerSelect
          useNativeAndroidPickerStyle={false}
          style={picketSelectStyles}
          onValueChange={(value) => setDatosFormulario({ ...datosFormulario, lugarEvento: value })} //Obtenemos el valor que se selecciona  y se guarda en el estado
          placeholder={
            {
              label: 'Selecciona el Lugar del Evento',
              value: 'Nulo'
            }
          }
          placeholderTextColor='#807e7e'
          items={[
            { label: 'Estadio Nemesio Diez', value: 'Estadio' },
          ]}
        />

        {/** Campo para Nombre del Evento */}
        <TextInput
          style={[styles.input, erroresFormulario.numeroElementos && styles.errorInput]} //Si erroresFormulario es True &&(Entonces) aplica el estilo
          placeholder='# Elementos de Seguridad'
          keyboardType="numeric"
          placeholderTextColor='#807e7e'
          onChange={(e) => onChange(e, 'numeroElementos')}
        />


        <View style={styles.view} >
          {/** Botón para Realizar Un registro */}
          <TouchableOpacity onPress={registrarEvento} >
            <Text style={styles.btnRegistrarEvento2}>Registrar Evento</Text>
          </TouchableOpacity>
        </View>


      </ScrollView>






      {/** Se prepara para mostrar el Picker Calendario*/}
      <DateTimePickerModal
        isVisible={isDatePickerVisible}  //Se muestra u oculta en base al cambio de estado
        mode='date'
        onConfirm={handlerConfirm}
        onCancel={hideDatePicker}
      />
    </>




  )
}

function valoresDefault() { /** Inicializa los valores que se recibirán del Formulario al Hook de Estado */
  return {
    idEvento: '',
    nombreEvento: '',
    fechaEvento: '',
    lugarEvento: 'Nulo',
    numeroElementos: '',

  }
}

const styles = StyleSheet.create({
  logo: { //40 160
    width: '40%',
    height: 160,
    borderRadius: 100,
    marginTop: 15,
    marginBottom: 20
  },
  scrollView: {
    width: '100%',
    marginTop: 70,
    marginBottom: 80,
    alignContent: 'center'
  },
  view: {
    flex: 1,
    alignItems: 'center'
  },
  input: {
    height: 50,
    color: '#000',
    width: '80%',
    marginLeft: 40,
    marginTop: 10,
    backgroundColor: '#f2f5f7',
    paddingHorizontal: 20,
    borderRadius: 50,
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#000'
  },
  datepicker: {
    justifyContent: 'center',
  },
  btnRegistrarEvento: {
    height: 50,
    paddingTop: 10,
    color: '#fff',
    paddingHorizontal: 20,
    fontSize: 18,
    backgroundColor: '#3b5998',   //Naranja: f28f24   Azul 0c527d
    borderRadius: 50,
    alignContent: 'center',
    borderColor: '#000',
    fontWeight: "bold",
  },
  btnRegistrarEvento2: {
    width: '50%',
    fontSize: 18,
    color: '#fff',
    backgroundColor: '#3b5998',
    height: 50,
    paddingTop: 10,
    paddingHorizontal: 20,
    borderRadius: 50,
    marginTop: 30,
  },
  mensajeError: {
    textAlign: 'center',
    color: '#f00',
    fontWeight: 'bold',
    fontSize: 20
  },
  errorInput: {
    borderColor: '#ff000d',
    borderWidth: 2,
  },


})



const picketSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
    backgroundColor: '#fff',
    marginLeft: -5,
    marginRight: -5
  },
  inputAndroid: {
    height: 50,
    color: '#000',
    width: '80%',
    marginLeft: 40,
    marginTop: 10,
    backgroundColor: '#f2f5f7',
    paddingHorizontal: 20,
    borderRadius: 50,
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#000'
  },
  errorInput: {
    borderColor: '#ff000d',
    borderWidth: 2,
  }
});