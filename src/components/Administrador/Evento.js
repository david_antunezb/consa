import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Modal, TouchableHighlight, Alert } from 'react-native'
import { Button, IconButton, Colors, Snackbar } from 'react-native-paper';

import firebase from '../../utils/firebase'
import 'firebase/firestore'

firebase.firestore().settings({ experimentalForceLongPolling: true })
const db = firebase.firestore(firebase);

export default function Evento(props) {
  // La propiedad irAConvocatoria solo se pasa si se desea redireccionar a la vista de convocar
  // Si no se pasa dicha propiedad, el componente funciona como 'solo lectura' y se puede reutilizar
  const { evento, irAConvocatoria, setReloadData } = props;
  const [fechaEvento, setFechaEvento] = useState('');
  //const [eventoSeleccionado, setEventoSeleccionado] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);

  const eventoPasado = evento.days > 0 ? true : false;   //True Mayor a cero, esta o es hoy el evento

  useEffect(() => {
    //console.log("QUE TRAE EVENTO");
    //console.log(evento);
    setFechaEvento(new Date(evento.fechaEvento.toDate())
      .toISOString().split('T')[0]);
  }, [])

  const infoDay = () => {
    if (evento.days == 0) {
      return (
        <View>
          <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#000' }} >Es Hoy</Text>
        </View>
      )
    } else {
      const days = -evento.days; // - * - Es mas, para que de positivos
      return (
        <View style={styles.textCurrent} >
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}> {days} </Text>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}> {days == 1 ? 'Dia' : 'Dias'} </Text>
        </View>
      );
    }
  }

  const convocarElementos = (evento) => {
    if (irAConvocatoria) {
      irAConvocatoria(evento);
    }
  }

  const eliminarEvento = (birthday) => {
    //console.log("Eliminando Evento...");
    Alert.alert(
      'Eliminar Evento',
      `¿Estas seguro de eliminar el evento ${evento.nombreEvento} ?`,
      [
        {
          text: 'Cancelar',
          style: 'cancel'
        }, //Boton de cancelar
        {
          text: 'Eliminar',
          onPress: () => {
            //Verificar que NO haya sido convocado
            var encontrado = false;

            db.collection('UsuariosEventos').where("idEvento", "==", evento.id)
              .get()
              .then((mensaje) => {
                mensaje.forEach((doc) => {
                  console.log("ENCONTRADO");
                  console.log(doc.data());
                  //setDatosUsuario(doc.data())
                  encontrado = true;
                });

                //console.log(mensaje);
                if (encontrado == true) {
                  console.log("UN EVENTO FUE ENCONTRADO - NO PUEDE SER ELIMINADO --------------");
                  Alert.alert(
                    'Error',
                    'Este Evento NO puede ser Eliminado'
                  );
                } else {
                  console.log("SE PUEDE ELIMINAR --------------------------------");
                  db.collection("Eventos")
                    .doc(evento.id)
                    .delete().then(() => {
                      Alert.alert(
                        'Aviso',
                        'El evento se ha eliminado'
                      );
                      setReloadData(true);
                    })
                }
              }).catch((error) => {
                console.log("ERROR");
                console.log(error);

              })




            //console.log("El Evento Ha Sido Eliminado: " + evento.id);
            /*db.collection("Eventos")
              .doc(evento.id)
              .delete().then(() => {
                setReloadData(true);
              })*/
          }
        }
      ],
      { cancelable: false },
    );
  }


  return (
    <>


      <View style={styles.viewRow}>
        {evento.days < 30 &&       //Muestra los Evento por venir, actuales y pasados de los últimos 30 días
          <>
            <TouchableOpacity
              onPress={() => convocarElementos(evento)}
              style={[
                styles.card,
                eventoPasado
                  ? styles.eventoPasado
                  : evento.days === 0
                    ? styles.hoy
                    : styles.current
              ]}
            >
              <View>
                <Text style={[styles.datosEvento, { fontWeight: 'bold', fontSize: 17 }]} >{evento.nombreEvento} </Text>
                <Text style={styles.datosEvento}>Fecha: {fechaEvento} </Text>
                <Text style={styles.datosEvento}># Elementos: {evento.numeroElementos} </Text>
              </View>


              {eventoPasado ? <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }} >Pasado</Text> : infoDay()
              }
            </TouchableOpacity >

            <View style={styles.btnDelete} >
              <IconButton
                icon="delete"
                color={Colors.white}
                size={40}
                onPress={() => eliminarEvento()}
              />
            </View>
          </>
        }





      </View>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Hello World!</Text>

            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <Text style={styles.textStyle}>Hide Modal</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    </>



  )
}

const mostrarModal = (evento) => {
  setEventoSeleccionado(evento);
  setShowModal(true);
};

const styles = StyleSheet.create({
  card: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 90,
    alignItems: 'center',
    paddingHorizontal: 10,
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 10,

    borderRadius: 15
  },
  eventoPasado: {
    backgroundColor: '#db7474'  //f2f5f7  820000  Rojo Tenue: d46868
  },
  hoy: {
    backgroundColor: '#97d1a2'   //559204
  },
  current: {
    backgroundColor: '#e0e381'  //1ae1f2
  },
  datosEvento: {
    color: '#000',
    fontSize: 16
  },
  textCurrent: {
    backgroundColor: '#fff',
    borderRadius: 30,
    width: 66,
    alignItems: 'center',
    justifyContent: 'center'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  viewRow: {
    flexDirection: 'row',
  },
  btnDelete: {
    //backgroundColor: "#fff",
    justifyContent: 'center',
    margin: -20,
  }
})
