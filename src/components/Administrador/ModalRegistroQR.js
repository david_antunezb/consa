import React, { useState, useEffect } from 'react';
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import { Button, IconButton, Colors, Snackbar } from 'react-native-paper';

import firebase from '../../utils/firebase';

export default function ModalSolicitud(props) {
  const db = firebase.firestore(firebase);
  const { showModal, setShowModal, usuarioSeleccionado, cambioPeticiones, setCambioPeticiones } = props;
  const [fechaNacimiento, setFechaNacimiento] = useState('');

  const [showSnack, setShowSnack] = useState(false);
  const onToggleSnackBar = () => setShowSnack(!showSnack);
  const onDismissSnackBar = () => setShowSnack(false);

  useEffect(() => {
    if (usuarioSeleccionado) {
      setFechaNacimiento(new Date(usuarioSeleccionado.fechaNacimiento.toDate())
        .toISOString().split('T')[0]);
    }
  }, [usuarioSeleccionado]);

  const contestarSolicitud = (aceptada) => {
    // si se acepta el estado cambia a CONFIRMADA, de lo contrario cambia a RECHAZADA
    const estado = aceptada ? 'CONFIRMADA' : 'RECHAZADA';
    db.collection('Usuarios').doc(usuarioSeleccionado.id).update({
      estadoSolicitud: estado
    }).then(() => {
      onToggleSnackBar();
      setCambioPeticiones(!cambioPeticiones);
      setShowModal(!showModal);
    }).catch((err) => {
      console.log(err);
    });
  }

  return (
    <>
      {/** Notificacion mostrada al atender una peticion */}
      <Snackbar
        visible={showSnack}
        onDismiss={onDismissSnackBar}
        action={{
          label: 'Aceptar',
          onPress: () => { }
        }}>
        Petición Atendida Correctamente!
      </Snackbar>

      {/** Modal con los datos del usuario */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={showModal}
        onRequestClose={() => {
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Image
              style={styles.userImage}
              source={require('../../assets/img/user-profile.png')} />

            <View style={styles.viewUserDetails}>
              <Text style={styles.modalText}>Nombre: {usuarioSeleccionado?.nombre}</Text>
              <Text style={styles.modalText}>Apellido Paterno: {usuarioSeleccionado?.aPaterno}</Text>
              <Text style={styles.modalText}>Apellido Materno: {usuarioSeleccionado?.aMaterno}</Text>
              <Text style={styles.modalText}>Fecha Nacimiento: {fechaNacimiento}</Text>
              <Text style={styles.modalText}>Puesto: {usuarioSeleccionado?.puesto}</Text>
              <Text style={styles.modalText}>Localidad: {usuarioSeleccionado?.zonaTrabajo}</Text>
            </View>

            <View style={styles.viewButtons}>
              <IconButton
                icon="delete"
                color={Colors.red500}
                size={70}
                onPress={() => contestarSolicitud(false)}
              />
              <IconButton
                icon="check-circle"
                color={Colors.green500}
                size={70}
                onPress={() => contestarSolicitud(true)}
              />
            </View>

            <Button color="#2196F3" mode="text" onPress={() => setShowModal(!showModal)}>
              Cerrar
            </Button>
          </View>
        </View>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000000aa',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: '85%'
  },
  textStyle: {
    color: '#2196F3',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  userImage: {
    width: 120,
    height: 120
  },
  viewUserDetails: {
    marginTop: 20
  },
  modalText: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  viewButtons: {
    flexDirection: 'row'
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
  }
});
