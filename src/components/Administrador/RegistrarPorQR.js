'use strict';
import React, { Component, useEffect, useState } from 'react';
import { StyleSheet, ActivityIndicator, View, PermissionsAndroid, Platform, Alert, Modal, Image } from 'react-native';
import {
  Provider as PaperProvider,
  Title,
  Text,
  TouchableOpacity,

} from 'react-native-paper';
import { Button, IconButton, Colors, Snackbar } from 'react-native-paper';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import Evento from './RegistroPorEvento';
import ModalSolicitud from './ModalRegistroQR';

/**IMPORTACIONES DE FIREBASE */
import firebase from '../../utils/firebase';
import 'firebase/firestore'
import { decode, encode } from 'base-64'
import 'firebase/auth';
//Acciones para Reconocer Firebase en Android
if (!global.btoa) global.btoa = encode;  //Para la base firebase
if (!global.atob) global.atob = decode;

firebase.firestore().settings({ experimentalForceLongPolling: true })
const db = firebase.firestore(firebase);

export default function RegistrarPorQr(props) {
  const { evento } = props.route.params;

  const [datosUsuario, setDatosUsuario] = useState({})
  const [modalVisible, setModalVisible] = useState(false);

  const [encontrado, setEncontrado] = useState(false);    //Se actualiza cuando el Empleado se encuentra Convocado en un evento
  const [cargando, setCargando] = useState(true);

  useEffect(() => {
    //limpiarEstados();
  }, [])

  const onSuccess = e => {
    //setCargando(true);
    console.log("MENSAJE LEIDO: " + e.data);
    buscarElemento(e.data);
    //setModalVisible(true)
  };

  const buscarElemento = (codigoQr) => { //En base al Código que Lee busca en la base de datos
    console.log("ID LEIDO: " + codigoQr);
    db.collection('Usuarios').where("idUsuario", "==", codigoQr)
      .get()
      .then((mensaje) => {
        mensaje.forEach(function (doc) {
          const usr = doc.data();
          usr.id = doc.id;
          setDatosUsuario(usr)
          //Buscar en la tabla de eventos convocados

          db.collection('UsuariosEventos').where("idEvento", "==", evento.id)
            .get()
            .then((mensaje) => {
              mensaje.forEach((doc) => {
                const convocatoria = doc.data();
                convocatoria.id = doc.id; //Obtenemos el id del Documento

                const arrayUsersEvents = convocatoria.usuariosEvento;

                for (let i = 0; i < arrayUsersEvents.length; i++) {
                  if (arrayUsersEvents[i].idUsuario == datosUsuario.id) {
                    setEncontrado(true);  //Indicamos que fue encontrado en la convocatoria
                    console.log(encontrado);
                    //setCargando(false);
                    break;
                  }

                }
                setCargando(false);
              });
              //setTimeOut(() => { setCargando(false) });
              //setCargando(false);
            });

        });

      });

    setModalVisible(true)
  }

  const registrarAsistencia = () => {
    console.log("Vamos a registrar en base");
    console.log("ID EVENTO: " + evento.id);
    console.log("ID usuario: " + datosUsuario.id);

    db.collection('AsistenciaEventos')   //Colección a la que voy a setear
      .add({
        idEmpleado: datosUsuario.id,
        idEvento: evento.id,
      })
      .then(() => {
        console.log("Se Registró con Éxito a la Tabla de Asistencia");
        //setModalVisible(false)
        limpiarEstados();
      })
      .catch((mensaje) => {
        console.log("ERROR");
        console.log(mensaje);
      })

  }

  const limpiarEstados = () => {
    setDatosUsuario({});
    setModalVisible(!modalVisible);
    setEncontrado(false);
    setCargando(true);
  }

  return (
    <>
      <PaperProvider>
        {/** Notificacion mostrada al atender una peticion */}


        <Title style={styles.title}>Registrar Asistencia</Title>
        {/* Datos generales del evento */}
        <Evento evento={evento}></Evento>
        {/* Contenedor para convocar a los empleados */}
        <QRCodeScanner
          style={styles.scanner}
          onRead={onSuccess}
          reactivate={true}
          flashMode={RNCamera.Constants.FlashMode.off}
          topContent={
            <Text style={styles.centerText}>
            </Text>
          }
        />
      </PaperProvider>


      {/** Modal con los datos del usuario */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>

            {cargando ? (
              <View style={styles.viewActivity}>
                <ActivityIndicator size="large" color="#000" />
              </View>
            ) : encontrado && !cargando ?
                (
                  <>
                    <Image
                      style={styles.userImage}
                      source={require('../../assets/img/user-profile.png')} />

                    <View style={styles.viewUserDetails}>
                      <Text style={styles.modalTextTitulo}>EMPLEADO ENCONTRADO</Text>

                      <View >
                        <View>
                          <Text style={styles.modalText}>Nombre: {datosUsuario.nombre} {datosUsuario.aPaterno} {datosUsuario.aMaterno} </Text>
                          <Text style={styles.modalText}>Puesto: {datosUsuario.puesto} </Text>
                          <Text style={styles.modalText}>Zona de trabajo: {datosUsuario.zonaTrabajo}</Text>
                        </View>
                      </View>

                      <Text style={styles.modalTextPregunta}> ¿Desea registrar asistencia? </Text>
                    </View>

                    <View style={styles.viewButtons}>
                      <IconButton
                        icon="delete"
                        color={Colors.red500}
                        size={70}
                        onPress={() => limpiarEstados()}
                      />
                      <IconButton
                        icon="check-circle"
                        color={Colors.green500}
                        size={70}
                        onPress={() => registrarAsistencia()}
                      />
                    </View>

                    <Button color="#3b5998" mode="text" onPress={() => limpiarEstados()}>
                      Cerrar
                  </Button>
                  </>
                ) : !encontrado && !cargando ? (
                  <>
                    <Image
                      style={styles.userImage}
                      source={require('../../assets/img/user-profile.png')} />

                    <View style={styles.viewUserDetails}>
                      <Text style={styles.modalUserFalse}>EMPLEADO NO ENCONTRADO</Text>
                    </View>

                    <Button color="#3b5998" mode="text" onPress={() => limpiarEstados()}>
                      Cerrar
                  </Button>

                  </>
                ) : (
                    <></>
                  )

            }
          </View>
        </View>
      </Modal>


    </>
  )
}

const styles = StyleSheet.create({
  title: {
    width: '90%',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 20,
    marginLeft: 20,
    borderBottomColor: '#fff',
    borderBottomWidth: 1,
    color: '#fff',
  },
  scanner: {
    backgroundColor: '#fff'
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)'
  },
  buttonTouchable: {
    padding: 16
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000000aa',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: '85%'
  },
  userImage: {
    width: 120,
    height: 120
  },
  viewUserDetails: {
    alignItems: 'center',
    marginTop: 20
  },
  viewButtons: {
    flexDirection: 'row'
  },
  modalText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000'
  },
  modalTextD: {
    fontSize: 16,
    color: '#000'
  },
  modalTextTitulo: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
    color: '#000'
  },
  rowDetails: {
    flexDirection: 'row',
  },
  modalTextPregunta: {
    marginTop: 30,
    fontSize: 18,
    fontWeight: 'bold',
    color: '#3b5998'
  },
  modalUserFalse: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 50,
    color: '#000'
  },
  viewActivity: {
    marginTop: '5%',
    alignItems: 'center',
  }

})
