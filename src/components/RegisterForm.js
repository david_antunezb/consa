import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, TextInput, ScrollView } from 'react-native'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import RNPickerSelect from 'react-native-picker-select';
import moment from 'moment';
import { validateEmail } from '../utils/validations'

/**IMPORTACIONES DE FIREBASE */
import firebase from '../utils/firebase'
import 'firebase/auth';
import 'firebase/firestore';

//Acciones para Reconocer Firebase en Android
firebase.firestore().settings({ experimentalForceLongPolling: true })

//Instanciamos la Base FIREBASE
const db = firebase.firestore(firebase)


export default function RegisterForm(props) {
  /**HACIENDO DESTRUCTURING */
  const { changeForm } = props;

  /**HOOK DE ESTADOS */
  const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);    //Hace visible el Calendario
  const [datosFormulario, setDatosFormulario] = useState(valoresDefault())  //Contiene los datos del formulario
  const [erroresFormulario, setErroresFormulario] = useState({});           //Contiene banderas para cada campo, True: Error, False: Correcto
  const [mensajeError, setMensajeError] = useState('');


  /** * * * * *  DEFINICIÓN DE FUNCIONES * * * * * */
  const hideDatePicker = () => { /**Oculta el Datepicker */
    setIsDatePickerVisible(false);
  };

  const showDatePicker = () => { /** Muestra el Datepicker */
    setIsDatePickerVisible(true);
  };

  const handlerConfirm = (date) => {/** Selecciona la fecha y se oculta el calendario */
    const fecha = date;
    fecha.setHours(0);
    fecha.setMinutes(0);
    fecha.setSeconds(0);

    setDatosFormulario({ ...datosFormulario, fechaNacimiento: fecha });
    hideDatePicker();
  };

  const onChange = (e, type) => { /** Función que guarda los datos del formulario en el Estado */
    setDatosFormulario({ ...datosFormulario, [type]: e.nativeEvent.text })

  };

  const registrarUsuario = () => {
    let errores = {};
    setErroresFormulario(errores);

    if (!datosFormulario.nombre || !datosFormulario.aPaterno || !datosFormulario.aMaterno || !datosFormulario.fechaNacimiento ||
      !datosFormulario.email || !datosFormulario.password || !datosFormulario.repeatPassword) {  //Si un campo es Vacío Entra

      if (!datosFormulario.nombre) errores.nombre = true;     //Si el campo esta vacío asigno True: Hay Error
      if (!datosFormulario.aPaterno) errores.aPaterno = true;
      if (!datosFormulario.aMaterno) errores.aMaterno = true;
      if (!datosFormulario.fechaNacimiento) errores.fechaNacimiento = true;
      if (!datosFormulario.email) errores.email = true;
      if (!datosFormulario.password) errores.password = true;
      if (!datosFormulario.repeatPassword) errores.repeatPassword = true;
      if (!datosFormulario.zonaTrabajo) errores.zonaTrabajo = true;
    }

    if (errores.nombre == true) {
      setMensajeError("Ingresa tu nombre");
    } else if (errores.aPaterno == true) {
      setMensajeError("Ingresa tu apellido paterno");
    } else if (errores.aMaterno == true) {
      setMensajeError("Ingresa tu apellido materno");
    } else if (errores.fechaNacimiento == true) {
      setMensajeError("Ingresa tu fecha de nacimiento");
    } else if (errores.email == true) {
      setMensajeError("Ingresa tu email");
    } else if (!validateEmail(datosFormulario.email)) { //Si el resultado es false entra
      setMensajeError("Ingresa un email válido");
    } else if (errores.password == true) {
      setMensajeError("Ingresa tu contraseña");
    } else if (datosFormulario.password != datosFormulario.repeatPassword) {
      setMensajeError("Contraseñas diferentes");
    } else if (datosFormulario.password.length < 6) {
      setMensajeError("Contraseña mínimo 6 caracteres");
    } else if (datosFormulario.zonaTrabajo == 'Nulo') {
      setMensajeError("Selecciona tu zona de trabajo")
    } else if (datosFormulario.puesto == 'Nulo') {
      setMensajeError("Selecciona tu puesto")
    }

    else { //Si todo está validado se Registra el Usuario

      setMensajeError("")

      //Se crea el usuario para AUTENTICACIÓN (Correo y Contraseña)
      firebase.auth().createUserWithEmailAndPassword(datosFormulario.email, datosFormulario.password)
        .then((response) => {
          var user = firebase.auth().currentUser;  //Obtenemos la info. del usuario que se acaba de crear

          datosFormulario.idEmpleado = user.uid;   //Asignamos el uid al campo idEmpleado del Estado datosFormulario

          //Se Llama a la Función que registra al usuario en la colección de Empleados
          agregarEmpleado(datosFormulario);

        }).catch((error) => {
          console.log(error);
          if (error.message == "The email address is already in use by another account.") {
            setMensajeError('El correo ya está ocupado, prueba otro');
            console.log("El correo ya está ocupado, prueba otro");
            setErroresFormulario({
              email: true
            });
          }

        })

    }

    //Si hay errores se setea el Hook de Estado
    setErroresFormulario(errores);
  }

  const agregarEmpleado = (datosFormulario) => {
    data = datosFormulario;
    db.collection('Usuarios')   //Colección a la que voy a setear
      .add({
        idUsuario: data.idEmpleado,
        nombre: data.nombre,
        aPaterno: data.aPaterno,
        aMaterno: data.aMaterno,
        fechaNacimiento: data.fechaNacimiento,
        email: data.email,
        password: data.password,
        zonaTrabajo: data.zonaTrabajo,
        estadoSolicitud: 'PENDIENTE',
        tipoUsuario: 'EMPLEADO',
        puesto: data.puesto,
      })
      .then(() => {
        console.log("Se Registró con Éxito a la Tabla Usuarios");
      })
      .catch((mensaje) => {
        console.log("ERROR");
        console.log(mensaje);
      })
  }


  return (
    <>
      <Text style={styles.titleForm} >Crear mi cuenta</Text>
      <Text style={styles.mensajeError} > {mensajeError} </Text>

      {/** Se genera un Scroll para el formulario */}
      <ScrollView style={styles.scrollView}>

        <Text style={styles.titleDatos} >Datos personales</Text>
        {/** Campo para Nombre del Empleado */}
        <TextInput
          style={[styles.input, erroresFormulario.nombre && styles.errorInput]} //Si erroresFormulario es True &&(Entonces) aplica el estilo
          placeholder='Nombre'
          placeholderTextColor='#807e7e'
          onChange={(e) => onChange(e, 'nombre')}
        />

        {/** Campo para Apellido Paterno del Empleado */}
        <TextInput
          placeholder="Apellido Paterno"
          placeholderTextColor='#807e7e'
          style={[styles.input, erroresFormulario.aPaterno && styles.errorInput]}
          onChange={(e) => onChange(e, 'aPaterno')}
        />

        {/** Campo para Apellido Materno del Empleado */}
        <TextInput
          placeholder="Apellido Materno"
          placeholderTextColor='#807e7e'
          style={[styles.input, erroresFormulario.aMaterno && styles.errorInput]}
          onChange={(e) => onChange(e, 'aMaterno')}
        />

        {/** Campo para Fecha de Nacimiento del Empleado */}
        <View style={[styles.input, styles.datepicker, erroresFormulario.fechaNacimiento && styles.errorInput]}>
          <Text style={{
            color: datosFormulario.fechaNacimiento ? '#000' : '#969696',
            fontSize: 18
          }} onPress={showDatePicker}>
            {datosFormulario.fechaNacimiento
              ? moment(datosFormulario.fechaNacimiento).format('LL')
              : 'Fecha de Nacimiento'
            }
          </Text>
        </View>


        <Text style={styles.titleDatos} >Datos de la cuenta</Text>

        {/** Campo para Correo Electrónico del Empleado */}
        <TextInput
          style={[styles.input, erroresFormulario.email && styles.errorInput]}
          placeholder='Correo Electrónico'
          placeholderTextColor='#807e7e'
          onChange={(e) => onChange(e, 'email')}
        />

        {/** Campo para Contraseña del Empleado */}
        <TextInput
          style={[styles.input, erroresFormulario.password && styles.errorInput]}
          placeholder='Contraseña'
          placeholderTextColor='#807e7e'
          secureTextEntry={true}
          onChange={(e) => onChange(e, 'password')}
        />

        {/** Campo para Repetir Contraseña del Empleado */}
        <TextInput
          style={[styles.input, erroresFormulario.repeatPassword && styles.errorInput]}
          placeholder='Repetir Contraseña'
          placeholderTextColor='#807e7e'
          secureTextEntry={true}
          onChange={(e) => onChange(e, 'repeatPassword')}
        />

        {/** Campo para Seleccionar zona de trabajo del Empleado */}
        <RNPickerSelect
          useNativeAndroidPickerStyle={false}
          style={picketSelectStyles}
          onValueChange={(value) => setDatosFormulario({ ...datosFormulario, zonaTrabajo: value })} //Obtenemos el valor que se selecciona  y se guarda en el estado
          placeholder={
            {
              label: 'Selecciona tu zona de trabajo',
              value: 'Nulo'
            }
          }
          placeholderTextColor='#807e7e'
          items={[
            { label: 'Cancha', value: 'Cancha' },
            { label: 'General', value: 'General' },
            { label: 'Palcos', value: 'Palcos' },
            { label: 'Preferente', value: 'Preferente' },
            { label: 'Tribuna Diablos', value: 'Tribuna Diablos' },

          ]}
        />

        {/** Campo para Seleccionar Puesto del Empleado */}
        <RNPickerSelect
          useNativeAndroidPickerStyle={false}
          style={picketSelectStyles}
          onValueChange={(value) => setDatosFormulario({ ...datosFormulario, puesto: value })} //Obtenemos el valor que se selecciona  y se guarda en el estado
          placeholder={
            {
              label: 'Selecciona tu puesto',
              value: 'Nulo'
            }
          }
          placeholderTextColor='#807e7e'
          items={[
            { label: 'Encargado de área', value: 'Encargado' },
            { label: 'Elemento de seguridad', value: 'Elemento' }

          ]}
        />
      </ScrollView>


      {/** Botón para Realizar Un registro */}
      <TouchableOpacity onPress={registrarUsuario} >
        <Text style={styles.btnRegistrarme}>Registrarme</Text>
      </TouchableOpacity>

      {/** Botón para Redireccionar a Login*/}
      <View style={styles.btnTextLogin}>
        <TouchableOpacity onPress={changeForm}>
          <Text style={styles.btnText} >Iniciar Sesión</Text>
        </TouchableOpacity>
      </View>




      {/** Se prepara para mostrar el Picker Calendario*/}
      <DateTimePickerModal
        isVisible={isDatePickerVisible}  //Se muestra u oculta en base al cambio de estado
        mode='date'
        onConfirm={handlerConfirm}
        onCancel={hideDatePicker}
      />
    </>
  )
}



function valoresDefault() { /** Inicializa los valores que se recibirán del Formulario al Hook de Estado */
  return {
    idEmpleado: null,
    nombre: '',
    aPaterno: '',
    aMaterno: '',
    fechaNacimiento: '',
    email: '',
    password: '',
    repeatPassword: '',
    zonaTrabajo: 'Nulo',
    estadoSolicitud: 'PENDIENTE',
    puesto: 'Nulo',
    activo: false,
  }
}


const styles = StyleSheet.create({
  titleForm: {
    color: '#000',
    fontSize: 24,
    fontFamily: "Cochin",
    fontWeight: "bold",
  },
  titleDatos: {
    marginTop: 10,
    width: '90%',
    marginLeft: 20,
    color: '#000',
    fontSize: 16,
    fontWeight: "bold",
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  scrollView: {
    marginBottom: 10,
    width: '100%',
  },
  scrollViewCompleto: {
    width: '100%',
  },
  input: {
    height: 50,
    color: '#000',
    width: '80%',
    marginLeft: 40,
    marginTop: 10,
    backgroundColor: '#f2f5f7',
    paddingHorizontal: 20,
    borderRadius: 50,
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#000'
  },
  datepicker: {
    justifyContent: 'center',
  },
  btnRegistrarme: {
    height: 50,
    paddingTop: 10,
    color: '#fff',
    paddingHorizontal: 20,
    fontSize: 18,
    backgroundColor: '#3b5998',   //Naranja: f28f24   Azul 0c527d
    width: '100%',
    borderRadius: 50,
    alignContent: 'center',
    borderColor: '#000',
    fontWeight: "bold",
  },
  btnTextLogin: {
    marginBottom: 20,
    marginTop: 20,
  },
  btnText: {
    color: '#3b5998',
    fontSize: 16,
    fontWeight: "bold",
  },
  errorInput: {
    borderColor: '#ff000d',
    borderWidth: 2,
  },
  mensajeError: {
    textAlign: 'center',
    color: '#f00',
    fontWeight: 'bold',
    fontSize: 20
  }

});

const picketSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
    backgroundColor: '#fff',
    marginLeft: -5,
    marginRight: -5
  },
  inputAndroid: {
    height: 50,
    color: '#000',
    width: '80%',
    marginLeft: 40,
    marginTop: 10,
    backgroundColor: '#f2f5f7',
    paddingHorizontal: 20,
    borderRadius: 50,
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#000'
  },
  errorInput: {
    borderColor: '#ff000d',
    borderWidth: 2,
  }
});

