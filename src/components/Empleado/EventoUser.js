import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Modal, TouchableHighlight, Alert, Image } from 'react-native'
import { Button, IconButton, Colors, Snackbar } from 'react-native-paper';

import firebase from '../../utils/firebase'
import 'firebase/firestore'

firebase.firestore().settings({ experimentalForceLongPolling: true })
const db = firebase.firestore(firebase);

export default function EventoUser(props) {
  // La propiedad irAConvocatoria solo se pasa si se desea redireccionar a la vista de convocar
  // Si no se pasa dicha propiedad, el componente funciona como 'solo lectura' y se puede reutilizar
  const { evento, irAConvocatoria, setCantidad, datosUsuario } = props;
  const [fechaEvento, setFechaEvento] = useState('');
  //const [eventoSeleccionado, setEventoSeleccionado] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);

  const eventoPasado = evento.days > 0 ? true : false;   //True Mayor a cero, esta o es hoy el evento

  useEffect(() => {
    //Tenemos datosUsiario.id -- 
    setFechaEvento(new Date(evento.fechaEvento.toDate())
      .toISOString().split('T')[0]);
  }, [])

  const infoDay = () => {
    if (evento.days == 0) {
      return (
        <View>
          <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#000' }} >Es Hoy</Text>
        </View>
      )
    } else {
      const days = -evento.days; // - * - Es mas, para que de positivos
      return (
        <View style={styles.textCurrent} >
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}> {days} </Text>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}> {days == 1 ? 'Dia' : 'Dias'} </Text>
        </View>
      );
    }
  }

  return (
    <>
      <View>
        {evento.days < 30 &&       //Muestra los Evento por venir, actuales y pasados de los últimos 30 días
          <>
            <TouchableOpacity
              style={[
                styles.card,
                eventoPasado
                  ? styles.eventoPasado
                  : evento.days === 0
                    ? styles.hoy
                    : styles.current
              ]}
            >
              <View>
                <Text style={[styles.datosEvento, { fontWeight: 'bold', fontSize: 17 }]} >{evento.nombreEvento} </Text>
                <Text style={styles.datosEvento}>Fecha: {fechaEvento} </Text>
                <Text style={styles.datosEvento}># Elementos: {evento.numeroElementos} </Text>
              </View>


              {eventoPasado ? <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }} >Pasado</Text> : infoDay()
              }
            </TouchableOpacity >


          </>
        }





      </View>
    </>



  )
}

const mostrarModal = (evento) => {
  setEventoSeleccionado(evento);
  setShowModal(true);
};

const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 90,
    alignItems: 'center',
    paddingHorizontal: 10,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    borderRadius: 15
  },
  eventoPasado: {
    backgroundColor: '#db7474'  //f2f5f7  820000  Rojo Tenue: d46868
  },
  hoy: {
    backgroundColor: '#97d1a2'   //559204
  },
  current: {
    backgroundColor: '#e0e381'  //1ae1f2
  },
  datosEvento: {
    color: '#000',
    fontSize: 16
  },
  textCurrent: {
    backgroundColor: '#fff',
    borderRadius: 30,
    width: 66,
    alignItems: 'center',
    justifyContent: 'center'
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000000aa',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: '85%'
  },
  userImage: {
    width: 120,
    height: 120
  },
  viewUserDetails: {
    alignItems: 'center',
    marginTop: 20
  },
  viewButtons: {
    flexDirection: 'row'
  },
  modalText: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  modalTextD: {
    fontSize: 18,
  },
  modalTextTitulo: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  rowDetails: {
    flexDirection: 'row',
  }
})

