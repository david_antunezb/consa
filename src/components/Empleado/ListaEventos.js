import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, ScrollView, ActivityIndicator } from 'react-native'
import { Provider as PaperProvider, Subheading, List, Badge } from 'react-native-paper'
import { View } from 'native-base'
import moment from 'moment';

import EventoCv from './EventoCv'


/**IMPORTACIONES DE FIREBASE */
import firebase from '../../utils/firebase';
import 'firebase/firestore'
import { decode, encode } from 'base-64'
import 'firebase/auth';

//Acciones para Reconocer Firebase en Android
if (!global.btoa) global.btoa = encode;  //Para la base firebase
if (!global.atob) global.atob = decode;

firebase.firestore().settings({ experimentalForceLongPolling: true })
const db = firebase.firestore(firebase);

export default function ListaEventos(props) {
  const { datosUsuario, setReloadData } = props;
  const [eventosConv, setEventosConv] = useState([])  //Contiene todos los Eventos Convocados DEL USUARIO LOGEADO
  const [cantidad, setCantidad] = useState(null)

  const [eventos, setEventos] = useState([])  //Contiene todos los Eventos de la base
  const [eventosPasados, setEventosPasados] = useState([])

  const [cargando, setCargando] = useState(true);

  useEffect(() => {
    setCargando(true);
    console.log("DATOS DEL USUARIO LOGUEADO *************************************************: " + datosUsuario.id);
    console.log(datosUsuario);
    obtenerEventos();
    db.collection('UsuariosEventos')
      .get()
      .then((mensaje) => {
        const itemsArray = []; //Se guardará la inforación obtenida
        mensaje.forEach((doc) => {
          const data = doc.data();
          data.id = doc.id; //Obtenemos el id del Documento (Sirve por si queremos eliminar)

          itemsArray.push(data);
        });
        guardarEventosConvocados(itemsArray);

      });


  }, [cantidad])

  const guardarEventosConvocados = (items) => {
    const arrayTempConvocatoria = [];
    const itemConvocatoria = {};
    items.forEach((item) => {
      let idEvento = item.idEvento;
      let usuariosEvento = item.usuariosEvento;

      usuariosEvento.map(function (obj) {
        console.log("ESTATUS: ", obj.statusConvocatoria);
        if (obj.idUsuario === datosUsuario.id && obj.statusConvocatoria === 'PENDIENTE') {

          itemConvocatoria.idEvento = idEvento;
          itemConvocatoria.idUsuario = obj.idUsuario;

          arrayTempConvocatoria.push(idEvento);
        }

      })

    })


    setEventosConv(arrayTempConvocatoria);
    setCantidad(eventosConv.length);

    console.log("******************************OBTENIENDO EVENTOS CONVOCADOS FINAL FINAL----------");
    console.log(eventosConv);
    console.log("--------");

    setCargando(false); //Cambiamos el status de carga
  }

  const obtenerEventos = () => {
    setEventos([]);
    db.collection('Eventos')
      .orderBy('fechaEvento', 'asc') //Primero las mas cercanas
      .get()
      .then((response) => {
        const itemsArray = []; //Se guardará la inforación obtenida
        response.forEach((doc) => {
          const data = doc.data();
          data.id = doc.id; //Obtenemos el id del Documento (Sirve por si queremos eliminar)

          itemsArray.push(data);
        });
        separarEventos(itemsArray);
      });
  }

  const separarEventos = (items) => {
    const fechaActual = moment().set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0
    });

    const arrayTempEventos = [];
    const arrayTempEventosPasados = [];

    items.forEach((item) => {
      const fecha = new Date(item.fechaEvento.seconds * 1000);
      const fechaEvento = moment(fecha);
      const anioActual = moment().get('year');
      fechaEvento.set({ year: anioActual });


      const diferenciaFecha = fechaActual.diff(fechaEvento, 'days')
      const itemTemp = item;

      itemTemp.fechaEvento = fechaEvento;
      itemTemp.days = diferenciaFecha;

      if (diferenciaFecha <= 0) {  //Los 0 o menos a cero -10 Significa que son los que estan por venir o es hoy el cumpleaños
        arrayTempEventos.push(itemTemp);
      } else {
        arrayTempEventosPasados.push(itemTemp);
      }
    })

    setEventos(arrayTempEventos);
    console.log("TODOS LOS EVENTOS -----------");
    console.log(eventos);
  }

  return (
    <View>
      <View style={styles.contenedorTitulo}>
        <Subheading style={styles.titulo}>
          Eventos Convocados:
            </Subheading>
        <Badge size={30}>{eventosConv.length}</Badge>
      </View>

      { cargando ?
        (
          <View style={styles.viewActivity}>
            <ActivityIndicator size="large" color="#fff" />
          </View>
        ) : cantidad && !cargando ?
          (
            <ScrollView style={styles.scrollView}>
              {eventos.map((item, index) => (
                eventosConv.map((item2, index2) => (

                  item.id == item2 ? <EventoCv key={index} evento={item} datosUsuario={datosUsuario} setCantidad={setCantidad} /> : console.log("NO HAY NADA: " + item2)


                ))
              ))}
            </ScrollView>
          ) : cantidad === 0 && !cargando ? (
            <>
              <View style={styles.contenedor}>
                <View style={styles.card}>
                  <Text style={styles.mensajeTitle}>Lista de convocatoria vacía</Text>
                  <Text style={styles.mensaje}>Por el momento no tienes Convocatorias disponibles.</Text>
                </View>
              </View>
              <View>
                <Text> </Text>
              </View>
            </>
          ) : (
              <></>
            )

      }


    </View >
  )
}

const styles = StyleSheet.create({
  contenedorTitulo: {
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: 20,
    flexDirection: 'row'
  },
  titulo: {
    color: '#fff',
    fontWeight: 'bold',
    marginRight: 10
  },
  viewActivity: {
    marginTop: '50%',
    alignItems: 'center',
  },
  contenedor: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  card: {
    backgroundColor: "#f2f5f7",   //e3e6e8
    width: '80%',
    height: 400,
    alignItems: 'center',
    marginTop: 100,
    borderRadius: 20,
    textAlign: 'center'
  },
  mensaje: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
    padding: 20
  },
  mensajeTitle: {
    fontSize: 26,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 100
  }
})
