import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Modal, TouchableHighlight, Alert, Image } from 'react-native'
import { Button, IconButton, Colors, Snackbar } from 'react-native-paper';

import firebase from '../../utils/firebase'
import 'firebase/firestore'

firebase.firestore().settings({ experimentalForceLongPolling: true })
const db = firebase.firestore(firebase);

export default function EventoCv(props) {
  // La propiedad irAConvocatoria solo se pasa si se desea redireccionar a la vista de convocar
  // Si no se pasa dicha propiedad, el componente funciona como 'solo lectura' y se puede reutilizar
  const { evento, irAConvocatoria, setCantidad, datosUsuario } = props;
  const [fechaEvento, setFechaEvento] = useState('');
  //const [eventoSeleccionado, setEventoSeleccionado] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);

  const eventoPasado = evento.days > 0 ? true : false;   //True Mayor a cero, esta o es hoy el evento
  const [showSnack, setShowSnack] = useState(false);
  const onToggleSnackBar = () => setShowSnack(!showSnack);
  const onDismissSnackBar = () => setShowSnack(false);


  useEffect(() => {
    //Tenemos datosUsiario.id -- 
    setFechaEvento(new Date(evento.fechaEvento.toDate())
      .toISOString().split('T')[0]);
  }, [])

  const infoDay = () => {
    if (evento.days == 0) {
      return (
        <View>
          <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#000' }} >Es Hoy</Text>
        </View>
      )
    } else {
      const days = -evento.days; // - * - Es mas, para que de positivos
      return (
        <View style={styles.textCurrent} >
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}> {days} </Text>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}> {days == 1 ? 'Dia' : 'Dias'} </Text>
        </View>
      );
    }
  }

  const contestarSolicitud = (aceptada) => {
    // si se acepta el estado cambia a CONFIRMADA, de lo contrario cambia a RECHAZADA
    const estado = aceptada ? 'CONFIRMADA' : 'RECHAZADA';
    console.log("LA SOLICITUD FUE: " + estado);
    console.log("ID DEL EVENTO: " + evento.id);
    var user = firebase.auth().currentUser;  //Obtenemos la info. del usuario que se acaba de crear
    console.log("USUARIO LOGEADO: " + user.uid);

    db.collection('UsuariosEventos').where("idEvento", "==", evento.id)
      .get()
      .then((mensaje) => {
        mensaje.forEach((doc) => {
          const convocatoria = doc.data();
          convocatoria.id = doc.id; //Obtenemos el id del Documento

          const arrayUsersEvents = convocatoria.usuariosEvento;

          arrayUsersEvents.map(function (ue) {
            if (ue.idUsuario == datosUsuario.id) {
              ue.statusConvocatoria = estado   //Agregamos El Nuevo Status de la Convocatoria
            }
          })

          convocatoria.usuariosEvento = arrayUsersEvents;
          actualizarBase(convocatoria)
        });
      });


  }

  const actualizarBase = (convocatoria) => {
    db.collection('UsuariosEventos').doc(convocatoria.id).update({
      idEvento: convocatoria.idEvento,
      usuariosEvento: convocatoria.usuariosEvento
    }).then(() => {
      Alert.alert(
        'Aviso',
        'Convocatoria Atendida Correctamente!'
      );
      //alert('Convocatoria Atendida Correctamente!');
      onToggleSnackBar();
      //setCambioPeticiones(!cambioPeticiones);
      setModalVisible(!modalVisible)
      setCantidad(null);//Para actualizar lista de Eventos
      console.log("SE HA MODIFICADO");
    }).catch((err) => {
      console.log(err);
    });
  }


  return (
    <>
      {/** Notificacion mostrada al atender una convocatoria */}
      <Snackbar
        visible={showSnack}
        onDismiss={onDismissSnackBar}
        action={{
          label: 'Aceptar',
          onPress: () => { }
        }}>
        Convocatoria Atendida Correctamente!
      </Snackbar>

      <View>
        {evento.days < 30 &&       //Muestra los Evento por venir, actuales y pasados de los últimos 30 días
          <>
            <TouchableOpacity
              onPress={() => setModalVisible(true)}
              style={[
                styles.card,
                eventoPasado
                  ? styles.eventoPasado
                  : evento.days === 0
                    ? styles.hoy
                    : styles.current
              ]}
            >
              <View>
                <Text style={[styles.datosEvento, { fontWeight: 'bold', fontSize: 17 }]} >{evento.nombreEvento} </Text>
                <Text style={styles.datosEvento}>Fecha: {fechaEvento} </Text>
                <Text style={styles.datosEvento}># Elementos: {evento.numeroElementos} </Text>
              </View>


              {eventoPasado ? <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }} >Pasado</Text> : infoDay()
              }
            </TouchableOpacity >


          </>
        }





      </View>

      {/** Modal con los datos del usuario */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Image
              style={styles.userImage}
              source={require('../../assets/img/icon-evento.png')} />

            <View style={styles.viewUserDetails}>
              <Text style={styles.modalTextTitulo}>DATOS DEL EVENTO</Text>

              <View style={styles.rowDetails} >
                <View>
                  <Text style={styles.modalText}>Evento: </Text>
                  <Text style={styles.modalText}>Fecha: </Text>
                  <Text style={styles.modalText}>Lugar: </Text>
                </View>
                <View>
                  <Text style={styles.modalTextD}> {evento.nombreEvento} </Text>
                  <Text style={styles.modalTextD}> {fechaEvento} </Text>
                  <Text style={styles.modalTextD}> {evento.lugarEvento} </Text>
                </View>

              </View>


            </View>

            <View style={styles.viewButtons}>
              <IconButton
                icon="delete"
                color={Colors.red500}
                size={70}
                onPress={() => contestarSolicitud(false)}
              />
              <IconButton
                icon="check-circle"
                color={Colors.green500}
                size={70}
                onPress={() => contestarSolicitud(true)}
              />
            </View>

            <Button color="#2196F3" mode="text" onPress={() => setModalVisible(!modalVisible)}>
              Cerrar
            </Button>
          </View>
        </View>
      </Modal>


    </>



  )
}

const mostrarModal = (evento) => {
  setEventoSeleccionado(evento);
  setShowModal(true);
};

const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 90,
    alignItems: 'center',
    paddingHorizontal: 10,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    borderRadius: 15
  },
  eventoPasado: {
    backgroundColor: '#db7474'  //f2f5f7  820000  Rojo Tenue: d46868
  },
  hoy: {
    backgroundColor: '#97d1a2'   //559204
  },
  current: {
    backgroundColor: '#e0e381'  //1ae1f2
  },
  datosEvento: {
    color: '#000',
    fontSize: 16
  },
  textCurrent: {
    backgroundColor: '#fff',
    borderRadius: 30,
    width: 66,
    alignItems: 'center',
    justifyContent: 'center'
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000000aa',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: '85%'
  },
  userImage: {
    width: 120,
    height: 120
  },
  viewUserDetails: {
    alignItems: 'center',
    marginTop: 20
  },
  viewButtons: {
    flexDirection: 'row'
  },
  modalText: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  modalTextD: {
    fontSize: 18,
  },
  modalTextTitulo: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  rowDetails: {
    flexDirection: 'row',
  }
})
