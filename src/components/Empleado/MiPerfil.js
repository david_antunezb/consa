import React, { useState, useEffect } from 'react'

import { StyleSheet, Text, View, Image, ScrollView } from 'react-native'

import { Provider as PaperProvider, Title } from 'react-native-paper'
import { Button, Card } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native'
/*Generacion de codigo qr**/
import QRCode from 'react-native-qrcode-svg';


/**IMPORTACIONES DE FIREBASE */
import firebase from '../../utils/firebase';
import 'firebase/firestore'
import { decode, encode } from 'base-64'
import 'firebase/auth';
//Acciones para Reconocer Firebase en Android
if (!global.btoa) global.btoa = encode;  //Para la base firebase
if (!global.atob) global.atob = decode;

firebase.firestore().settings({ experimentalForceLongPolling: true })
const db = firebase.firestore(firebase);



export default function MiPerfil(props) {
  const { user } = props;
  const [usuario, setUsuario] = useState(undefined)
  const [datosUsuario, setDatosUsuario] = useState({})



  useEffect(() => {
    var user = firebase.auth().currentUser;  //Obtenemos la info. del usuario que se acaba de acceder
    setUsuario(user);

    var email = user.email;
    db.collection('Usuarios').where("email", "==", email)
      .get()
      .then((mensaje) => {
        mensaje.forEach((doc) => {
          const usr = doc.data();
          usr.id = doc.id;
          setDatosUsuario(usr)
          console.log(usr);
        });
      });
  }, [])

  return (
    <PaperProvider>

      <View style={styles.otherContainer}>
        <Title style={[styles.textThemeDark, styles.title]}>Mi Perfil</Title>
      </View>

      {/** Se genera un Scroll para Mi Perfil de Empleado */}
      <ScrollView style={styles.scrollView}>
        <View style={styles.datosEmpleado}>
          <View style={styles.containerImage}>
            <Image
              style={styles.userImage}
              source={require('../../assets/img/user-profile.png')} />
          </View>


          <Text style={[styles.headline2, { fontWeight: 'bold', fontSize: 17 }]}>Nombre: {datosUsuario.nombre} </Text>
          <Text style={[styles.headline2, { fontWeight: 'bold', fontSize: 17 }]}>Apellido Paterno: {datosUsuario.aPaterno} </Text>
          <Text style={[styles.headline2, { fontWeight: 'bold', fontSize: 17 }]}>Apellido Materno: {datosUsuario.aMaterno} </Text>
          <Text style={[styles.headline2, { fontWeight: 'bold', fontSize: 17 }]}>Puesto: {datosUsuario.puesto} </Text>
          <Text style={[styles.headline2, { fontWeight: 'bold', fontSize: 17 }]}>Zona Trabajo: {datosUsuario.zonaTrabajo} </Text>
          <View style={styles.otherContainer}>
            <QRCode
              value={firebase.auth().currentUser.uid}
              logo={require('../../assets/img/LogoConsa.png')} // or logo={{uri: base64logo}}
              size={240}
              logoMargin={2}
              logoSize={60}
              logoBorderRadius={10}
            />
          </View>
        </View>

      </ScrollView>



    </PaperProvider>
  )
}

const styles = StyleSheet.create({
  scrollView: {
    marginBottom: 10,
    width: '100%',
  },
  viewClose: {
    backgroundColor: '#B20000',
    borderRadius: 50,
    paddingVertical: 10,
    paddingHorizontal: 30
  },
  textThemeDark: {
    color: '#fff',
  },
  title: {
    width: '90%',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 10,
    marginLeft: 20,
    borderBottomColor: '#fff',
    borderBottomWidth: 1,

  },
  headline: {
    color: 'white',
    fontSize: 18,
    marginTop: 0,
    alignItems: 'flex-start',
  },
  headline2: {
    color: 'black',
    padding: 5,
    fontSize: 16,
    marginTop: 0,
    alignItems: 'flex-start',
  }
  ,
  otherContainer: {
    alignItems: 'center',
    marginTop: 20
  },
  datosEmpleado: {
    backgroundColor: "#f2f5f7",   //e3e6e8
    margin: 20,
    padding: 15,
    paddingBottom: 40,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  userImage: {
    width: 120,
    height: 120,
  },
  containerImage: {
    alignItems: 'center',
  }

})
