import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, ScrollView, Image, Dimensions } from 'react-native'
import { Title } from "react-native-paper";
import CarouselVertical from "../Empleado/CarouselVertical";

import firebase from '../../utils/firebase'


// import { Provider as PaperProvider, Title } from 'react-native-paper'
import { Button, Card } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native'

const { width } = Dimensions.get('window');
const height = width * 0.8; //70%
const images = [
  'https://n9.cl/7mar',
  'https://n9.cl/l7s3r',
  'https://n9.cl/y9j5t',
  'https://n9.cl/v6llj',
  'https://n9.cl/r1ehj',
]


export default function HomeEmpleado(props) {

  const { user } = props;
  const [newEvent, setNewEvent] = useState(null);
  //console.log("Usuario recibido: ", user);


  return (
    <>
      <Text style={[styles.textThemeDark, styles.title]}> </Text>
      <Text style={[styles.textThemeDark, styles.title]}>B i e v e n i d o</Text>
      <Text style={[styles.textThemeDark, styles.title]}>E V E N T O S   CONSA</Text>
      <View style={styles.container}>


        <ScrollView pagingEnabled horizontal style={styles.scroll}
          showsVerticalScrollIndicator={false}
        >
          {
            images.map((image, index) => (
              <Image
                key={index}
                source={{ uri: image }}
                style={styles.image}//contain
              />

            ))
          }
        </ScrollView>

        <View style={styles.dot}>
          {
            images.map((i, k) => (
              <Text key={k} style={styles.pagText}>⬤</Text>
            ))
          }


        </View>





        <CarouselVertical data={newEvent} />
      </View>

      <ScrollView showsVerticalScrollIndicator={false}>
        {newEvent && (
          <View style={styles.news}>
            <Title style={styles.newsTitle}>Holiii</Title>
          </View>
        )}
      </ScrollView>

    </>
  );

}

const styles = StyleSheet.create({
  container: { marginTop: 20, width, height },
  scroll: { width, height },
  image: { width, height, resizeMode: 'contain' }, //cover
  pagText: { color: '#888', margin: 3 },




  viewClose: {
    backgroundColor: '#B20000',
    borderRadius: 50,
    paddingVertical: 10,
    paddingHorizontal: 30,

  },
  textThemeDark: {
    color: '#fff',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 20,
    alignSelf: "center",
    //marginBottom: 15,
  },
  news: {
    marginVertical: 10,
  },
  newsTitle: {
    marginBottom: 15,
    marginHorizontal: 20,
    fontWeight: "bold",
    fontSize: 22,
  },
  dot: {
    flexDirection: "row",
    position: "absolute",
    bottom: 0,
    alignSelf: "center"
  }

})

