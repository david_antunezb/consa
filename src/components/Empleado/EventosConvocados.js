import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, ActivityIndicator } from 'react-native'
import { Provider as PaperProvider, Subheading, List, Badge } from 'react-native-paper'
import { View } from 'native-base'
import ListaEventos from './ListaEventos'


/**IMPORTACIONES DE FIREBASE */
import firebase from '../../utils/firebase';
import 'firebase/firestore'
import { decode, encode } from 'base-64'
import 'firebase/auth';

//Acciones para Reconocer Firebase en Android
if (!global.btoa) global.btoa = encode;  //Para la base firebase
if (!global.atob) global.atob = decode;

firebase.firestore().settings({ experimentalForceLongPolling: true })
const db = firebase.firestore(firebase);


export default function EventosConvocados(props) {
  const { } = props;
  const [eventos, setEventos] = useState(null); //cambiar a null cuando lo jale de la BD
  const [eventosConv, setEventosConv] = useState([])  //Contiene todos los Eventos Convocados
  const [usuario, setUsuario] = useState(undefined)
  const [datosUsuario, setDatosUsuario] = useState(undefined)
  const [estatus, setEstatus] = useState('PENDIENTE')
  const [bandera, setBandera] = useState(false)
  //console.log("Usuario recibido: ", user);
  const [reloadData, setReloadData] = useState(false);

  useEffect(() => { //Consultamos los datos del usuario
    var user = firebase.auth().currentUser;  //Obtenemos la info. del usuario que se acaba de crear
    setUsuario(user);
    console.log("INFO DE USUARIO");
    console.log(user);

    var email = user.email;
    db.collection('Usuarios').where("email", "==", email)
      .get()
      .then((mensaje) => {
        mensaje.forEach((doc) => {
          const usr = doc.data();
          usr.id = doc.id;
          console.log("INFORMACIÓN DEL USUARIO LOGEADO");
          console.log(usr);
          setDatosUsuario(usr)

        });
      });

    setReloadData(false);
  }, [reloadData])

  if (datosUsuario === undefined) return null;

  return (
    <PaperProvider>

      { datosUsuario.estadoSolicitud == 'CONFIRMADA' ?
        <>
          <ListaEventos datosUsuario={datosUsuario} setReloadData={setReloadData} />

        </>
        :
        datosUsuario.estadoSolicitud == 'PENDIENTE' ?
          <>
            <View style={styles.contenedor}>
              <View style={styles.card}>
                <Text style={styles.mensajeTitle}>Tu solicitud esta siendo procesada.</Text>
                <Text style={styles.mensaje}>Por el momento NO puedes ver tus eventos.</Text>
              </View>
            </View>
          </>
          :
          datosUsuario.estadoSolicitud == 'RECHAZADA' &&
          <>
            <View style={styles.contenedor}>
              <View style={styles.card}>
                <Text style={styles.mensajeTitle}>Tu solicitud ha sido RECHAZADA.</Text>
                <Text style={styles.mensaje}>NO puedes ser convocado a eventos</Text>
              </View>
            </View>
          </>
      }

    </PaperProvider>
  )
}

const styles = StyleSheet.create({
  viewClose: {
    height: 40,
    paddingTop: 7,
    paddingLeft: 35,
    color: '#FFF',
    paddingHorizontal: 20,
    fontSize: 18,
    backgroundColor: '#B20000',
    width: '50%',
    borderRadius: 50,
    alignContent: 'center',
    borderColor: '#000',
    fontWeight: "bold",


    //paddingVertical: 10,
    //paddingHorizontal: 30,
  },
  contenedorTitulo: {
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: 20,
    flexDirection: 'row'
  },
  titulo: {
    color: '#fff',
    fontWeight: 'bold',
    marginRight: 10
  },
  viewContainer: {
    padding: 20,
  },
  contenedor: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  card: {
    backgroundColor: "#f2f5f7",   //e3e6e8
    width: '80%',
    height: 400,
    alignItems: 'center',
    marginTop: 100,
    borderRadius: 20,
    textAlign: 'center'
  },
  mensajeTitle: {
    fontSize: 26,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 100
  },
  mensaje: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
    padding: 20
  }
})
