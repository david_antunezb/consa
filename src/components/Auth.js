import React, { useState } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

/**IMPORTACIONES DE COMPONENTES */
import LoginForm from './LoginForm';
import RegisterForm from './RegisterForm';

/** COMPONENTE Auth
 * Se encargará de mostrar el formulario de login o registro
 */
export default function Auth() {
  /**HOOK DE ESTADOS */
  const [isLogin, setIsLogin] = useState(true) //INDICARÁ EL FORMULARIO QUE DEBE RENDERIZARCE

  const changeForm = () => {
    /** Se encarga de cambiar el estado para poder renderizar Login o Registro */
    setIsLogin(!isLogin);
  }

  return (
    <View style={styles.view}>

      { isLogin ?
        <>
          <Text style={styles.titleWelcome} >Bienvenido</Text>
          <Text style={styles.titleAppLogin} >C O N S A   App</Text>
          {/** Se agrega la imagen a la vista */}
          <Image
            style={styles.logo}
            source={require('../assets/img/LogoConsa.png')}
          />

        </> :
        <>
          <Text style={styles.titleAppRegister} >C O N S A   App</Text>
          <Image
            style={styles.logoRegistro}
            source={require('../assets/img/LogoConsa.png')}
          />

        </>
      }




      {/** Verifica el estado del usuario.  Si isLogin=true se muestra el Formulario de login SINO el Formulario de Registro */}
      { isLogin ? <LoginForm changeForm={changeForm} /> : <RegisterForm changeForm={changeForm} />}
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center'
  },
  titleAppLogin: {
    marginTop: 10,
    color: '#000',
    fontSize: 35,
    fontFamily: "Cochin",
    fontWeight: "bold",
  },
  titleAppRegister: {
    color: '#000',
    fontSize: 35,
    fontFamily: "Cochin",
    fontWeight: "bold",
  },
  titleWelcome: {
    marginTop: 70,
    color: '#000',
    fontSize: 30,
    fontFamily: "Cochin"
  },
  logo: { //40 160
    width: '40%',
    height: 160,
    borderRadius: 100,
    marginTop: 15,
    marginBottom: 20
  },
  logoRegistro: {
    width: '40%',
    height: 160,
    borderRadius: 100,
    marginTop: 5,
    marginBottom: 10
  }

})
